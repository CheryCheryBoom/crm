/**
 * Created by dima on 13.02.15.
 */
angular.module('bos.crm.report').directive('bosPieChart', function () {
    return {
        restrict: 'A',
        scope: {
            data: "="
        },
        link: function (scope, elem, attrs, ctrl) {

                console.log(scope.chart);
                console.log("init new chart");
                scope.chart = AmCharts.makeChart(elem[0], {
                    "type": "pie",
                    "theme": "dark",
                    "dataProvider": scope.data,
                    "valueField": "value",
                    "titleField": "country",
                    "outlineAlpha": 0.4,
                    "depth3D": 15,
                    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
                    "angle": 30,
                    "exportConfig": {
                        menuItems: [{
                            icon: '/lib/3/images/export.png',
                            format: 'png'
                        }]
                    }
                });



            scope.chart.animateAgain();
            console.log(scope.chart);
            //jQuery('.chart-input').off().on('input change', function () {
            //    var property = jQuery(this).data('property');
            //    var target = chart;
            //    var value = Number(this.value);
            //    chart.startDuration = 0;
            //
            //    if (property == 'innerRadius') {
            //        value += "%";
            //    }
            //
            //    target[property] = value;
            //    chart.validateNow();
            //});
        }
    }
});