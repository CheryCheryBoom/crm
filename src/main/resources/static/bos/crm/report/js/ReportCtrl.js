/**
 * Created by dmytro on 18.07.14.
 */
angular.module('bos.crm.report').controller('ReportCtrl', function ($scope) {
    $scope.lazyDirectiveConfig = {name: 'bos.crm.report', files: ['/bos/crm/report/js/ChartsDirectives.js']};

    $scope.data = [{
        "country": "Lithuania",
        "value": 260
    }, {
        "country": "Ireland",
        "value": 201
    }, {
        "country": "Germany",
        "value": 65
    }, {
        "country": "Australia",
        "value": 39
    }, {
        "country": "UK",
        "value": 19
    }, {
        "country": "Latvia",
        "value": 10
    }];
});