/**
 * Created by dmytro on 18.07.14.
 */
angular.module('bos.crm.report').directive('bosPlotPieChart', function ($http, $rootScope) {
    return{
        restrict: 'A',
        link: function (scope, element, attrs) {
            var data = [];

            if (!attrs.radius)attrs.radius = 1;
            if (!attrs.tilt)attrs.tilt = 0.5;

            $http.post(attrs.source, {group: attrs.group}).success(function (resp) {
                angular.forEach(resp, function (value, key) {
                    data.push({label: $rootScope.i18n[attrs.descr][value[0]].ua + '[' + value[1] + ']', data: value[1]});
                });

                $.plot(element, data, {
                    series: {
                        pie: {
                            show: true,
                            radius: attrs.radius,
                            tilt: attrs.tilt,
                            label: {
                                radius: 0.7,
                                show: true,
                                combine: {
                                    color: '#999',
                                    threshold: 0.1
                                }
                            },
                            combine: {
                                color: '#999',
                                threshold: 0
                            }
                        }
                    },
                    legend: {
                        show: true
                    },
                    grid: {
                        hoverable: true,
                        clickable: true
                    }
                });
            });
        }
    };
});