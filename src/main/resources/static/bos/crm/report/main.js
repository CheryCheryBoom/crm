'use strict';
/**
 * Created by dmytro on 23.08.14.
 * Changed by dima on 13.02.15.
 */
angular.module('bos.crm.report', ['bos.common']).config(function ($stateProvider) {
    var commonFiles = [
        '../../../assets/admin/pages/css/profile.css',
        "../../assets/global/plugins/amcharts/amcharts/whaaat.css",

        //"../../assets/global/plugins/amcharts/amcharts/amcharts.js",
        //"../../assets/global/plugins/amcharts/amcharts/serial.js",
        //"../../assets/global/plugins/amcharts/amcharts/pie.js",
        //"../../assets/global/plugins/amcharts/amcharts/radar.js",
        //"../../assets/global/plugins/amcharts/amcharts/themes/light.js",
        //"../../assets/global/plugins/amcharts/amcharts/themes/patterns.js",
        //"../../assets/global/plugins/amcharts/amcharts/themes/chalk.js",
        //"../../assets/global/plugins/amcharts/ammap/ammap.js",
        //"../../assets/global/plugins/amcharts/ammap/maps/js/worldLow.js",
        //"../../assets/global/plugins/amcharts/amstockcharts/amstock.js",
        //"../../assets/global/plugins/amcharts/amcharts/themes/dark.js",

        //"/bos/crm/report/js/ChartsDirectives.js",

        '../../../assets/admin/pages/scripts/profile.js'
    ];

    $stateProvider
        .state('report', { //shows deal table
            url: "/reports",
            templateUrl: "/bos/crm/report/views/index.html",
            data: {pageTitle: 'Reports'},
            controller: "ReportCtrl",
            ncyBreadcrumb: {
                label: 'Reports'
            },
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'bos.crm.report',
                        insertBefore: '#ng_load_plugins_before',
                        //serie:true,
                        files: _.union(commonFiles, [
                            '/bos/crm/report/js/ReportCtrl.js'
                        ])
                    });
                }]
            }
        })
});
