angular.module('bos.crm.lead').controller('FileUploadCtrl', ['$scope', '$http', function ($scope, $http) {
        console.log('We are in controller');
        var url = '/bos/api/uploader';
        $scope.options = {
            url: url,
            dataType: 'json'
        };

        var additionalFieldsSize = 0;
        $scope.importData = [];
        $scope.getAdditionalSize = function () {
            var arr = [];
            for(var k = 0; k < additionalFieldsSize; k++) {
                arr.push(k);
            }
            return arr;
        }

        $scope.addAdditionalField = function () {
            if (!additionalFieldsSize) {
                var buttonForm = document.getElementsByName('buttonsBodyHolder')[0]
                buttonForm.style['margin-bottom'] = '20px';

                var additionalFiledForm = document.getElementsByName('additionalFieldsBodyHolder')[0]
                additionalFiledForm.style['padding-top'] = '10px'
                additionalFiledForm.style['padding-bottom'] = '10px'
            }
            additionalFieldsSize++
            $scope.importData.push({})
        }

        $scope.removeAdditionalField = function () {
            if(additionalFieldsSize) {
                additionalFieldsSize--
                $scope.importData.pop()
                if (!additionalFieldsSize) {
                    var buttonForm = document.getElementsByName('buttonsBodyHolder')[0]
                    buttonForm.style['margin-bottom'] = '0px';

                    var additionalFiledForm = document.getElementsByName('additionalFieldsBodyHolder')[0]
                    additionalFiledForm.style['padding-top'] = '0px';
                    additionalFiledForm.style['padding-bottom'] = '0px';
                }
            }
        }

        $scope.upload = function (fileList) {

            var fd = new FormData();
            for (var i = 0; i < $scope.files.length; i++) {

                // get item
                file = $scope.files.item(i);
                fd.append('file' + i, file);
            }

            $http.post(url, fd,
                {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                }).progress(function (evt) {
                    console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
                }).success(function (data) {
                    console.log(data)
                })
        }

        $scope.import = function () {
            if ($scope.files.length == 1) {
                var fd = new FormData();
                // get item
                file = $scope.files.item(0);
                //check that file name has xls extension
                if (file.name && ~file.name.indexOf('.xls')) {
                    fd.append('table', file);
                    //make input form invisible
                    var result = 'Added Fields : \''
                    for(var key in $scope.importData) {
                        if($scope.importData[key].tableHeader && $scope.importData[key].saveAs) {
                            result += $scope.importData[key].tableHeader + ':' + $scope.importData[key].saveAs
                            fd.append('additionalColumn' + key, JSON.stringify($scope.importData[key]))
                        }
                    }
                    result += '\''
                    console.log(result)

                    $http.post('/bos/api/uploader/importdata', fd,
                        {
                            transformRequest: angular.identity,
                            headers: {'Content-Type': undefined}
                        }
                    ).success(function (data) {
                            console.log(data)
                            //toast.info(data)
                            document.location.replace( "/" )

                        }).error(function (data) {
//                            toast.info('Exception during parsing of document')
                            console.log('Exception during parsing of document\n' + data)
                        })
                } else {
                    $scope.files = undefined
                    console.log('Not a xls document.')
//                    toast.info('Not a xls document.')
                }
            } else {
                console.log('Wrong file length : ' + $scope.files.length)
            }
        }

        $scope.execute = function (mapping) {
            console.log(mapping)
            $http.post('/bos/api/uploader/executedata.json', mapping)
        }

    }]);