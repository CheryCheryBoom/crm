angular.module('bos.crm.lead').directive('fileInput', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, elm, attrs) {
                elm.bind('change', function () {
                    console.log('Directive works');
                    $parse(attrs.fileInput).assign(scope, elm[0].files)
                    scope.$apply()
                })
            }
        }
    }])
