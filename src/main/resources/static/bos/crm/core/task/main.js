'use strict';
/**
 * Created by dmytro on 23.08.14.
 */
angular.module('bos.crm.task', ['bos.common']).config(function ($stateProvider) {

    //files for layout and style, from metronic template
    var commonFiles = [
        '../../../assets/global/plugins/bootstrap-datepicker/css/datepicker3.css',
        '../../../assets/global/plugins/select2/select2.css',
        '../../../assets/admin/pages/css/todo.css',
        '../../assets/global/plugins/fullcalendar/fullcalendar.min.css',

        '../../../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
        '../../../assets/global/plugins/select2/select2.min.js',

        '../../../assets/admin/pages/scripts/todo.js'
    ];
    $stateProvider
        .state('task', { //shows task table
            url: "/task",
            templateUrl: "assets/bos/crm/core/task/views/index.html",
            data: {pageTitle: 'Tasks'},
            controller: "ListTaskCtrl",
            ncyBreadcrumb: {
                label: 'Tasks'
            },
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'bos.crm.task',
                        insertBefore: '#ng_load_plugins_before',
                        files: _.union(commonFiles, [
                            'assets/bos/crm/core/task/js/ListTaskCtrl.js'
                        ])
                    });
                }]
            }
        })
        .state("task.show", {
            url: "/{id:int}",
            views: {
                "@": {
                    templateUrl: "assets/bos/crm/core/task/views/show.html",
                    controller: "ShowTaskCtrl"
                }
            },
            data: {pageTitle: 'Task Profile'},
            ncyBreadcrumb: {
                label: '{{task.title}}'
            },
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'bos.crm.task',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: _.union(commonFiles, [
                            'assets/bos/crm/core/task/js/ShowTaskCtrl.js'
                        ])
                    });
                }],
                task: ['$stateParams', 'Restangular', function ($stateParams, Restangular) {
                    return Restangular.one('bos/crud/task', $stateParams.id).get();
                }]
            }
        })

        .state("task.show.activity", {
            url: '/activity',
            templateUrl: "assets/bos/crm/audit/views/list.html",
            controller: 'AuditCtrl',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'bos.crm.audit',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'assets/bos/crm/audit/js/AuditCtrl.js'
                        ]
                    });
                }],
                preset: function (task) {
                    return [
                        {fld: 'task.id', op: 'eq', val: task.id}
                    ];
                }
            }})

        .state("task.schedule", {
            url: "/schedule",
            views: {
                "@": {
                    templateUrl: "assets/bos/crm/common/schedule/schedule.html",
                    controller: "ScheduleCtrl"
                }
            },
            data: {pageTitle: 'Task Schedule'},
            ncyBreadcrumb: {
                label: 'Schedule'
            },
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'bos.crm.task',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: _.union(commonFiles, [
                            'assets/bos/crm/common/schedule/ScheduleCtrl.js'
                        ])
                    });
                }],
                task: ['$stateParams', 'Restangular', function ($stateParams, Restangular) {
                    return Restangular.one('bos/crud/task', $stateParams.id).get();
                }]
            }
        })

        .state("task.edit", {
            url: "/{id:int}/edit",
            views: {
                "@": {
                    templateUrl: "assets/bos/crm/core/task/views/edit.html",
                    controller: "EditTaskCtrl"
                }
            },
            data: {pageTitle: 'Edit Task'},
            ncyBreadcrumb: {
                label: '{{task.title}}'
            },
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'bos.crm.task',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: _.union(commonFiles, [
                            'assets/bos/crm/core/task/js/EditTaskCtrl.js'
                        ])
                    });
                }],
                task: ['$stateParams', 'Restangular', function ($stateParams, Restangular) {
                    return Restangular.one('bos/crud/task', $stateParams.id).get();
                }]
            }
        })
        // Task Profile Dashboard
        /*.state("task.show.edit", {
         url: "/edit",
         templateUrl: "/bos/crm/core/task/views/edit.html",
         controller: 'EditTaskCtrl',
         data: {pageTitle: 'Task Profile Edit'}
         })*/

        //Task Profile Account
        .state("task.new", {
            abstract: true,
            url: "/new",
            views: {
                "@": {
                    templateUrl: "assets/bos/crm/core/task/views/profile.html",
                    controller: 'EditTaskCtrl'
                }
            },
            data: {pageTitle: 'Add new Task'},
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'bos.crm.task',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: _.union(commonFiles, [
                            'assets/bos/crm/core/task/js/EditTaskCtrl.js'
                        ])
                    });
                }],
                task: function (Restangular) {
                    return Restangular.one('bos/crud/task');//{contact:{fields:[]}};
                    //task.contact = {fields:[]};
                    //return task;
                }
            }
        })
        // Task Profile Dashboard
        .state("task.new.edit", {
            url: "/edit",
            templateUrl: "assets/bos/crm/core/task/views/edit/account.html",
            data: {pageTitle: 'Task Profile Edit'}
        })

        .state('calendar', {
            url: "/calendar",
            templateUrl: 'assets/bos/crm/core/task/calendar/calendar.html',
            controller: 'ListTaskCtrl',
            data: {pageTitle: 'Calendar'},
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'bos.crm.task',
                            insertBefore: '#ng_load_plugins_before',
                            files: _.union(commonFiles, [
                            'assets/bos/crm/core/task/js/ListTaskCtrl.js'
                            ])
                        });
                    }]
        }
    });
});

    /* ----------------- timeline ---------------- */
    /*        when('/calendar', {
     templateUrl: '/bos-app/task/calendar/calendar.html'
     //controller: 'CalendarCtrl'
     }).
     when('/timeline', {
     templateUrl: '/bos-app/task/calendar/timeline.html'
     //controller: 'CalendarCtrl'
     }).*/
    /* ---------------- tasks ------------------ */
    /* when('/add-task', {
     templateUrl: '/bos-app/task/bricks/account.html',
     controller: 'EditTaskCtrl'
     }).
     when('/edit-task/:taskId', {
     templateUrl: '/bos-app/task/bricks/account.html',
     controller: 'EditTaskCtrl',
     resolve: {
     task: function ($q, $route, taskService) {
     var deferred = $q.defer();
     taskService.find($route.current.pathParams.taskId)
     .then(function (event) {
     deferred.resolve(event);
     });
     return deferred.promise;
     }
     }
     }).
     when('/list-task', {
     templateUrl: '/bos-app/task/bricks/list.html',
     controller: 'ListTaskCtrl'
     }).
     when('/list-project', {
     templateUrl: '/bos-app/task/project/list.html',
     controller: 'ListProjectCtrl'
     }).
     when('/show-task/:taskId', {
     templateUrl: '/bos-app/task/bricks/show.html',
     controller: 'ShowTaskCtrl',
     resolve: {
     task: function ($q, $route, taskService) {
     var deferred = $q.defer();
     taskService.find($route.current.pathParams.taskId)
     .then(function (event) {
     deferred.resolve(event);
     });
     return deferred.promise;
     }
     }
     });
     });*/
/*});*/
   /* angular.module('bos.crm.task').directive('bosSubtasks', function () {
        return{
            restrict: 'A',
            templateUrl: '/bos/crm/core/task/views/index.html',
            scope: true,
            controller: 'ListTaskCtrl'
        }
    });

    angular.module('bos.crm.task').directive('bosParticipants', function () {
        return{
            restrict: 'A',
            templateUrl: '/bos/crm/core/task/directives/participants.html',
            scope: true,
            controller: function ($scope, participationService, paging) {
                paging.init(participationService, $scope, $scope.participationPreset);
                $scope.filter.sort = 'participant.contact.fullName';
                $scope.filter.max = 100;
                $scope.reload();

                $scope.add = function (part) {
                    part.task = {id: $scope.task.id};
                    if (part)
                        alarmService.save(part).then(function () {
                            $scope.reload();
                            $scope.alarm = {};
                        });
                };

                $scope.remove = function (part) {
                    alarmService.remove(part).then(function () {
                        $scope.reload();
                    });
                };
            }
        }
    });

    angular.module('bos.crm.task').directive('bosAlarms', function () {
        return{
            restrict: 'A',
            templateUrl: '/bos/crm/core/task/directives/alarms.html',
            scope: true,
            controller: function ($scope, alarmService, paging) {
                $scope.alarm = {alarmDate: new Date($scope.task.startDate.getTime() - 10 * 60000)};

                paging.init(alarmService, $scope, $scope.alarmPreset);
                $scope.filter.sort = 'alarmDate';
                $scope.filter.max = 100;
                $scope.reload();

                $scope.add = function (alarm) {
                    alarm.task = {id: $scope.task.id};
                    if (alarm)
                        alarmService.save(alarm).then(function () {
                            $scope.reload();
                            $scope.alarm = {alarmDate: new Date($scope.task.startDate.getTime() - 10 * 60000)};
                        });
                };

                $scope.remove = function (alarm) {
                    alarmService.remove(alarm).then(function () {
                        $scope.reload();
                    });
                };
            }
        }
    });


    angular.module('bos.crm.task').directive('bosCalendar', function (taskService, bosDate) {
        return{
            restrict: 'A',
            scope: {},
            link: function (scope, element) {
                scope.calendar = [];

                taskService.findAll({composite: []}).$promise.then(function (tasks) {

                    angular.forEach(tasks.data, function (value, key) {
                        this.push({
                            title: value.title,
                            url: '/show-task/' + value.id,
                            start: bosDate.parse(value.startDate),
                            end: bosDate.parse(value.dueDate),
                            backgroundColor: '#3366FF'//Metronic.getBrandColor('blue')
                        })
                    }, scope.calendar);

                    element.fullCalendar({
                        lang: 'uk',
                        header: {
                            right: 'title',
                            center: '',
                            left: 'agendaDay, agendaWeek, month, today, prev, next',
                            today: 'Сьогодні',
                            month: 'Місяць',
                            week: 'Тиждень',
                            day: 'День'
                        },
                        events: scope.calendar,
                        timeFormat: 'H(:mm)'
                    });
                });
            }
        }
    });
*/