/**
 * Created by dmytro on 24.09.14.
 */
/**
 * Created by dmytro on 27.05.14.
 */
angular.module('bosServices').service('participationService', function ($resource, $q) {
    var resource = $resource('/bos/crud/participation/:id', {id: '@id'},
        {
            update: {method: 'PUT'},
            filter: {url: '/bos/filter/participation', method: 'POST'}
        });
    return {
        findAll: function (filter) {
            return resource.filter(filter);
        },
        save: function (participation) {
            var deferred = $q.defer();
            resource.save(participation,
                function (response) {
                    deferred.resolve(response);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        },
        update: function (participation) {
            var deferred = $q.defer();
            resource.update(participation,
                function (response) {
                    deferred.resolve(response);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        },
        remove: function (participation) {
            var deferred = $q.defer();
            resource.delete(participation,
                function (response) {
                    deferred.resolve(response);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        }
    };
});