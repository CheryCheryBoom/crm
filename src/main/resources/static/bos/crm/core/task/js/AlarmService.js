/**
 * Created by dmytro on 24.09.14.
 */
/**
 * Created by dmytro on 27.05.14.
 */
angular.module('bosServices').service('alarmService', function ($resource, $q, bosDate) {
    var resource = $resource('/bos/crud/alarm/:id', {id: '@id'},
        {
            update: {method: 'PUT'},
            filter: {url: '/bos/filter/alarm', method: 'POST'}
        });
    return {
        findAll: function (filter) {
            return resource.filter(filter);
        },
        byManager: function (managerId) {
            var deferred = $q.defer();
            var filter = {composite: [
                {'and': [
                    {fld: 'target.id', op: 'eq', val: managerId}
                ]}
            ]};
            resource.filter(filter,
                function (response) {
                    deferred.resolve(response.data);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        },
        save: function (alarm) {
            alarm.alarmDate = bosDate.format(alarm.alarmDate);
            var deferred = $q.defer();
            resource.save(alarm,
                function (response) {
                    deferred.resolve(response);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        },
        update: function (alarm) {
            alarm.alarmDate = bosDate.format(alarm.alarmDate);
            var deferred = $q.defer();
            resource.update(alarm,
                function (response) {
                    deferred.resolve(response);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        },
        remove: function (alarm) {
            var deferred = $q.defer();
            resource.delete(alarm,
                function (response) {
                    deferred.resolve(response);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        }
    };
});