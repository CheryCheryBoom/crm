/**
 * Created by dmytro on 30.05.14.
 */
angular.module('bos.crm.task').controller('ShowTaskCtrl', function ($scope, task) {
    $scope.task = task; //$route.current.locals.task;
   /* $scope.task.startDate = bosDate.parse($scope.task.startDate);
    $scope.task.dueDate = bosDate.parse($scope.task.dueDate);*/
    //$scope.alarms = taskService.findAlarms($scope.task.id);

    $scope.$on('$viewContentLoaded', function() {
        Metronic.initAjax(); // initialize core components
    });

    $scope.taskPreset = [
        {fld: 'supertask.id', op: 'eq', val: $scope.task.id}
    ];

    /*$scope.commentPreset = [
        {fld: 'task.id', op: 'eq', val: $scope.task.id}
    ];*/

    $scope.auditPreset = [
        {fld: 'task.id', op: 'eq', val: $scope.task.id}
    ];

/*    $scope.participationPreset = [
        {fld: 'task.id', op: 'eq', val: $scope.task.id}
    ];

    $scope.alarmPreset = [
        {fld: 'task.id', op: 'eq', val: $scope.task.id}
    ];*/

    //$scope.findManagerByName = managerService.findAllByName;

    //$scope.checkTask = taskService.checkTask;
    $scope.tab = 0;
});