/**
 * Created by dmytro on 21.07.14.
 */
angular.module('bos.crm.task').controller('ListTaskCtrl', function ($scope, $http, paging, $window) {
/*    if ($routeParams.type) {
        if ($scope.taskPreset)
            $scope.taskPreset.push({fld: 'type', op: 'eq', val: $routeParams.type});
        else $scope.taskPreset = [
            {fld: 'type', op: 'eq', val: $routeParams.type}
        ];
    }*/

    paging.init('task', $scope, $scope.taskPreset);
    $scope.reload();

    $scope.report = function (format) {
        $window.location = '/export/task.' + format;
    };

    $scope.fireFilter = function (text) {
        $scope.filter.composite = [
            {
                "or": [
                    {"fld": "title", "op": "ilike", "val": '%' + text + '%'}
                ]
            }
        ];
        $scope.pager.currentPage = 1;
        $scope.reload();
    };

    //$scope.calendar = {date: new Date()};

    /*$scope.isDone = function (task) {
        return task.status == $rootScope.i18n.taskStatuses.SUCCESS.name || task.status == $rootScope.i18n.taskStatuses.FAILURE.name
    };

    $scope.isDanger = function (task) {
        return bosDate.now().isAfter(task.dueDate) && !$scope.isDone(task);
    };*/

//    $scope.$watch('calendar.date', function (newVal, oldVal) {
//        $scope.tasks = taskService.findAllByManagerAndDate($rootScope.profile.id, newVal);
//    });

   /* $scope.setStatus = function (task, status) {
        task.status = status.name;
        taskService.update(task);
    };*/

    $scope.setPriority = function (task, priority) {
        task.priority = priority.value;
        //todo taskService.update(task);
    };

    //$scope.checkTask = taskService.checkTask;
});