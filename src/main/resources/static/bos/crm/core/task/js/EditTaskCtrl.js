/**
 * Created by dmytro on 30.05.14.
 */
angular.module('bos.crm.task').controller('EditTaskCtrl', function (task, paging, $state, $modal, $scope, Restangular, $location, $http, $filter) {
    if (task) {
        $scope.task = task; //$route.current.locals.task;
    }
    else {
        $scope.task = {
            responsible: $rootScope.profile,//todo {id: $rootScope.profile.id, fullName: $rootScope.profile.fullName},
            startDate: new Date(),
            type: $routeParams.type,
            priority: 50
        };

        $scope.task.startDate.setMinutes($scope.task.startDate.getMinutes() - $scope.task.startDate.getMinutes() % 5 + 5);
        $scope.task.dueDate = new Date($scope.task.startDate.getTime() + 10 * 60000);
/*        if ($routeParams.supertask) {
            taskService.find($routeParams.supertask).then(function (resp) {
                $scope.task.supertask = resp;
                $scope.task.lead = resp.lead;
                $scope.task.deal = resp.deal;
                //1006133
            });
        }
        if ($routeParams.deal)
            dealService.find($routeParams.deal).then(function (data) {
                $scope.task.deal = data;
                $scope.task.lead = data.lead;
            });
        if ($routeParams.lead && !$scope.task.lead)
            leadService.find($routeParams.lead).then(function (data) {
                $scope.task.lead = data;
            });*/
    }

    /*$scope.findManagerByName = managerService.findAllByName;
    $scope.findLeadByName = leadService.findAllByName;
    $scope.findTaskByName = taskService.findAllByName;
    $scope.findDealByNameAndLead = dealService.findAllByNameAndLead;*/

/*    $scope.selectLead = function (lead) {
        if ($scope.task.deal && lead.id != $scope.task.deal.lead.id) {
            $scope.task.deal = null;
        }
    };

    $scope.selectDeal = function (deal) {
        if (!$scope.task.lead || !deal.lead.id != $scope.task.lead.id) {
            $scope.task.lead = deal.lead;
        }
    };*/

    $scope.save = function ( task) {
/*        if (task.id)
            taskService.update(task).then(callback);
        else
            taskService.save(task).then(callback);
        $scope.cancel();*/

        task.fields = _.reject(task.fields, function (field) {
            return !field.value;
        });

        task.save();
    };

    //$scope.checkTask = taskService.checkTask;

/*    $scope.sendMail = function (dataForm, task) {
        $scope.save(dataForm, task, function (data) {
            $http.post('/bos/api/task/send-mail/' + data.id)
                .success(function (data) {
                    toast.success('Листа успішно відправлено');
                });
        });
    };*/

    $scope.tab = 0;
    $scope.taskType = 'TASK';
});