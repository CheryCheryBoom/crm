/**
 * Created by dmytro on 30.05.14.
 */
angular.module('bosServices').service('taskService', function ($resource, $q, $route, $location, $modal, bosDate) {
    var resource = $resource('/bos/crud/task/:id', {id: '@id'},
        {
            update: {method: 'PUT'},
            filter: {url: '/bos/filter/task', method: 'POST'}
        });
    return{
        find: function (taskId) {
            var deferred = $q.defer();
            resource.get({id: taskId},
                function (data) {
                    data.startDate = bosDate.parse(data.startDate);
                    data.dueDate = bosDate.parse(data.dueDate);
                    deferred.resolve(data);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        },
        findAllByName: function (name) {
            var deferred = $q.defer();
            resource.filter({
                    config: 'search',
                    composite: [
                        {"or": [
                            {fld: "title", op: 'ilike', val: '%' + name + '%'}
                        ]}
                    ]},
                function (data) {
                    deferred.resolve(data.data);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        },
        findAll: function (filter) {
            return resource.filter(filter);
        },
        save: function (task) {
            task.startDate = bosDate.format(task.startDate);
            task.dueDate = bosDate.format(task.dueDate);
            var deferred = $q.defer();
            resource.save(task,
                function (response) {
                    deferred.resolve(response);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        },
        update: function (task) {
            task.startDate = bosDate.format(task.startDate);
            task.dueDate = bosDate.format(task.dueDate);
            var deferred = $q.defer();
            resource.update(task,
                function (response) {
                    deferred.resolve(response);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        },
        checkTask: function (task) {
            var modalInstance = $modal.open({
                templateUrl: '/bos-app/task/bricks/check.html',
                controller: function ($scope, $modalInstance, taskService, task) {
                    $scope.task = task;
                    $scope.check = {status: 'SUCCESS'};
                    $scope.actions = {nextTask: ''};

                    $scope.saveResult = function () {
                        $scope.task.status = $scope.check.status;
                        taskService.update($scope.task).then(function () {
                            $modalInstance.close({task: $scope.task, actions: $scope.actions});
                        });
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                },
                backdrop: 'static',
                //size: 'lg','sm'
                resolve: {
                    task: function () {
                        task.startDate = new Date(task.startDate);
                        task.dueDate = new Date(task.dueDate);
                        return task;
                    }
                }
            });

            modalInstance.result.then(function (response) {
                var toLead = '';
                if (response.task.lead)
                    toLead = 'lead=' + response.task.lead.id;
                switch (response.actions.nextTask) {
                    case '':
                        $route.reload();
                        break;
                    case 'DEAL':
                        $location.url('/add-deal?' + toLead);
                        break;
                    default:
                        $location.url('/add-task?' + toLead + '&type=' + response.actions.nextTask);
                        break;
                }
            });
        }
    }
});