/**
 * Created by dmytro on 04.08.14.
 */
angular.module('bos.crm.manager').controller('ListManagerCtrl', function ($scope, $http, paging, $window) {
    paging.init('manager', $scope, $scope.managerPreset);
    $scope.reload();

    $scope.report = function (format) {
        $window.location = '/export/manager.' + format;
    };

    $scope.fireFilter = function (text) {
        $scope.filter.composite = [
            {
                "or": [
                    {"fld": "contact.fullName", "op": "ilike", "val": '%' + text + '%'}
                ]
            }
        ];
        $scope.pager.currentPage = 1;
        $scope.reload();
    };

    //$scope.$watch('pager.simple', function (now) {
    //    $scope.fireFilter(now);
    //});

});