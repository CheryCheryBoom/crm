/**
 * Created by dmytro on 30.05.14.
 */
angular.module('bosServices').service('managerService', function ($resource, $q) {
    var resource = $resource('/bos/crud/manager/:id', {id: '@id'},
        {
            me: {url: '/bos/api/manager/me'},
            role: {url: '/bos/api/manager/set-role/:id', params: {id: '@id'}, method: 'POST'},
            update: {method: 'PUT'},
            filter: {url: '/bos/filter/manager', method: 'POST'}
        });
    return{
        myProfile: function () {
            var deferred = $q.defer();
            resource.me({},
                function (data) {
                    deferred.resolve(data);
                }, function (resp) {
                    deferred.reject(resp);
                });
            return deferred.promise;
        },
        find: function (managerId) {
            var deferred = $q.defer();
            resource.get({id: managerId},
                function (data) {
                    deferred.resolve(data);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        },
        findAll: function (filter) {
            return resource.filter(filter);
        },
        findAllByName: function (name) {
            var deferred = $q.defer();
            resource.filter({composite: [
                    {"or": [
                        {"fld": "contact.fullName", "op": "ilike", "val": '%' + name + '%'}
                    ]}
                ]},
                function (data) {
                    deferred.resolve(data.data);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        },
        save: function (manager) {
            var deferred = $q.defer();
            resource.save(manager,
                function (data) {
                    deferred.resolve(data);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        },
        update: function (manager) {
            var deferred = $q.defer();
            resource.update(manager,
                function (data) {
                    deferred.resolve(data);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        },
        setRole: function (manager, role) {
            var deferred = $q.defer();
            resource.role({id: manager.id, role: role},
                function (data) {
                    deferred.resolve(data);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        }
    };
});