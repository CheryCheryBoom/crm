'use strict';
/**
 * Created by dmytro on 23.08.14.
 */
angular.module('bos.crm.manager', ['bos.common']).config(function ($stateProvider) {

    //files for layout and style, from metronic template
    var profileCommonFiles = [
        '../../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
        '../../../assets/admin/pages/css/profile.css',
        '../../../assets/admin/pages/css/tasks.css',

        '../../../assets/global/plugins/jquery.sparkline.min.js',
        '../../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',

        '../../../assets/admin/pages/scripts/profile.js'//,
        //'/bos/crm/core/actors/common/ContactDirectives.js'
    ];

    $stateProvider
        .state('manager', { //shows manager table
            url: "/manager",
            templateUrl: "/bos/crm/core/actors/manager/views/index.html",
            data: {pageTitle: 'Managers'},
            controller: "ListManagerCtrl",
            ncyBreadcrumb: {
                label: 'Managers'
            },
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'bos.crm.manager',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            '/bos/crm/core/actors/manager/js/ListManagerCtrl.js'
                        ]
                    });
                }]
            }
        })
        .state("manager.show", {
            url: "/{id:int}",
            views: {
                "@": {
                    templateUrl: "/bos/crm/core/actors/manager/views/profile.html",
                    controller: "ShowManagerCtrl"
                }
            },
            data: {pageTitle: 'Manager Profile'},
            ncyBreadcrumb: {
                label: '{{manager.contact.fullName}}'
            },
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'bos.crm.manager',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: _.union(profileCommonFiles, [
                            '/bos/crm/core/actors/manager/js/ShowManagerCtrl.js',
                            '/bos/crm/core/actors/manager/js/EditManagerCtrl.js'
                        ])
                    });
                }],
                manager: ['$stateParams', 'Restangular', function ($stateParams, Restangular) {
                    if ($stateParams.id > 0) {
                        return Restangular.one('bos/crud/manager', $stateParams.id).get();
                    } else {
                        var manager = Restangular.one('bos/crud/manager');
                        manager.contact = {fields: []};
                        return manager;
                    }
                }]
            }
        })
        // Manager Profile Dashboard
        .state("manager.show.edit", {
            url: "/edit",
            templateUrl: "/bos/crm/core/actors/manager/views/edit/account.html",
            controller: 'EditManagerCtrl',
            data: {pageTitle: 'Manager Profile Edit'}
        })

        .state("manager.show.dashboard", {
            url: '/home',
            templateUrl: "/bos/crm/core/actors/manager/views/show/dashboard.html"
        })

        .state("manager.show.activity", {
            url: '/activity',
            templateUrl: "/bos/crm/audit/views/list.html",
            controller: 'AuditCtrl',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'bos.crm.audit',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            '/bos/crm/audit/js/AuditCtrl.js'
                        ]
                    });
                }],
                preset: function (manager) {
                    return [
                        {fld: 'author.id', op: 'eq', val: manager.id},
                        {fld: 'task.author.id', op: 'eq', val: manager.id},
                        {fld: 'task.responsible.id', op: 'eq', val: manager.id}
                    ];
                }
            }})

        .state("manager.show.edit.info", {
            url: "/info",
            templateUrl: "/bos/crm/core/actors/manager/views/edit/info.html"
        })
        .state("manager.show.edit.other", {
            url: "/other",
            templateUrl: "/bos/crm/core/actors/manager/views/edit/other.html"
        });

    //Manager Settings
    /*        .state("manager.settings",{
     url: "/settings",
     templateUrl: "/bos/crm/core/actors/manager/views/edit/settings.html",
     data: {pageTitle: 'Manager Profile Settings'}
     });*/
    /*when('/settings', {
     templateUrl: '/bos-app/actors/manager/bricks/edit/settings.html',
     controller: 'EditSettingsCtrl',
     resolve: {
     manager: function ($q, $route, managerService) {
     var deferred = $q.defer();
     managerService.myProfile()
     .then(function (data) {
     deferred.resolve(data);
     });
     return deferred.promise;
     }
     }
     });*/
});