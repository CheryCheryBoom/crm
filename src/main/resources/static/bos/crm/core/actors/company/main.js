'use strict';
/**
 * Created by dmytro on 23.08.14.
 */
angular.module('bos.crm.company', ['bos.common']).config(function ($stateProvider) {

    //files for layout and style, from metronic template
    var profileCommonFiles = [
        '../../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
        '../../../assets/admin/pages/css/profile.css',
        '../../../assets/admin/pages/css/tasks.css',

        '../../../assets/global/plugins/jquery.sparkline.min.js',
        '../../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',

        '../../../assets/admin/pages/scripts/profile.js'//,
        //'/bos/crm/core/actors/common/ContactDirectives.js'
    ];

    $stateProvider
        .state('company', { //shows company table
            url: "/company",
            templateUrl: "/bos/crm/core/actors/company/views/index.html",
            data: {pageTitle: 'Company'},
            controller: "ListCompanyCtrl",
            ncyBreadcrumb: {
                label: 'Company'
            },
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'bos.crm.company',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            '/bos/crm/core/actors/company/js/ListCompanyCtrl.js'
                        ]
                    });
                }]
            }
        })
        .state("company.show", {
            url: "/{id:int}",
            views: {
                "@": {
                    templateUrl: "/bos/crm/core/actors/company/views/profile.html",
                    controller: "ShowCompanyCtrl"
                }
            },
            data: {pageTitle: 'Company Profile'},
            ncyBreadcrumb: {
                label: '{{company.title}}'
            },
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'bos.crm.company',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: _.union(profileCommonFiles, [
                            '/bos/crm/core/actors/company/js/ShowCompanyCtrl.js',
                            '/bos/crm/core/actors/company/js/EditCompanyCtrl.js'
                        ])
                    });
                }],
                company: ['$stateParams', 'Restangular', function ($stateParams, Restangular) {
                    if ($stateParams.id > 0) {
                        return Restangular.one('bos/crud/company', $stateParams.id).get();
                    } else {
                        var company = Restangular.one('bos/crud/company');
                        company.fields = [];
                        return company;
                    }
                }]
            }
        })
        // Company Profile Dashboard
        .state("company.show.edit", {
            abstract: true,
            url: "/edit",
            templateUrl: "/bos/crm/core/actors/company/views/edit/account.html",
            controller: 'EditCompanyCtrl',
            data: {pageTitle: 'Company Profile Edit'}
        })

        .state("company.show.dashboard", {
            url: '/home',
            templateUrl: "/bos/crm/core/actors/company/views/show/dashboard.html"
        })

        .state("company.show.activity", {
            url: '/activity',
            templateUrl: "/bos/crm/audit/views/list.html",
            controller: 'AuditCtrl',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'bos.crm.audit',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            '/bos/crm/audit/js/AuditCtrl.js'
                        ]
                    });
                }],
                preset: function (company) {
                    return [
                        {fld: 'lead.company.id', op: 'eq', val: company.id},
                        {fld: 'task.lead.company.id', op: 'eq', val: company.id},
                        {fld: 'deal.lead.company.id', op: 'eq', val: company.id}
                    ];
                }
            }})

        .state("company.show.edit.info", {
            url: "/info",
            templateUrl: "/bos/crm/core/actors/company/views/edit/info.html"
        })
        .state("company.show.edit.other", {
            url: "/other",
            templateUrl: "/bos/crm/core/actors/company/views/edit/other.html"
        });
});

/*
 angular.module('bosCompany').filter('dealAmount', function ($rootScope) {
 return function (amount) {
 if (amount > 0) {
 return $rootScope.i18n.domain.company.good;
 } else {
 return $rootScope.i18n.domain.company.bad;
 }
 }
 });

 angular.module('bosCompany').filter('dealAmountText', function ($rootScope) {
 return function (amount) {
 if (amount > 0) {
 return $rootScope.i18n.domain.company.goodText;
 } else {
 return $rootScope.i18n.domain.company.badText;
 }
 }
 });*/
