/**
 * Created by dmytro on 18.07.14.
 */
angular.module('bos.crm.company').controller('ShowCompanyCtrl', function ($scope, company) {
    $scope.company = company; // $route.current.locals.company;

    $scope.$on('$viewContentLoaded', function() {
        Metronic.initAjax(); // initialize core components
    });

    $scope.auditPreset = [
        {fld: 'lead.company.id', op: 'eq', val: $scope.company.id},
        {fld: 'task.lead.company.id', op: 'eq', val: $scope.company.id},
        {fld: 'deal.lead.company.id', op: 'eq', val: $scope.company.id}
    ];

    $scope.leadPreset = [
        {fld: 'company.id', op: 'eq', val: $scope.company.id}
    ];

//    $http.get('/lead/by-company/' + $scope.company.id).success(function (data) {
//        $scope.leads = data;
//    });

/*    $scope.cancel = function () {
        $route.reload();
    };*/

    $scope.tab = 0;

//    $scope.tab = [
//        {active: true}
//    ];

//    $scope.infoTabs = [
//        '/bos-app/actors/company/bricks/show/account.html',
//        '/bos-app/audit/bricks/feeds.html',
//        '/bos-app/actors/company/bricks/show/leads.html'
//    ];
});