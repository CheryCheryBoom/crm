/**
 * Created by dmytro on 27.05.14.
 */
angular.module('bosServices').service('companyService', function ($resource, $q, $location) {
    var resource = $resource('/bos/crud/company/:id', {id: '@id'},
        {
            update: {method: 'PUT'},
            filter: {url: '/bos/filter/company', method: 'POST'}
        });
    return {
        find: function (id) {
            var deferred = $q.defer();
            resource.get({id: id},
                function (data) {
                    deferred.resolve(data);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        },
        findAll: function (filter) {
            return resource.filter(filter);
        },
        findAllByName: function (name) {
            var deferred = $q.defer();
            var filter = {
                config: 'search',
                composite: [
                    {"or": [
                        {"fld": "title", "op": "ilike", "val": '%' + name + '%'}
                    ]}
                ]};
            resource.filter(filter,
                function (data) {
                    deferred.resolve(data.data);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        },
        save: function (item) {
            var deferred = $q.defer();
            resource.save(item,
                function (response) {
                    deferred.resolve(response);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        },
        update: function (item) {
            var deferred = $q.defer();
            resource.update(item,
                function (response) {
                    deferred.resolve(response);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        },
        show: function (companyId) {
            $location.url('/show-company/' + companyId);
        }
    };
});