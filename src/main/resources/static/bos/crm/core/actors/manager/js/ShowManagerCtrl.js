/**
 * Created by dmytro on 04.08.14.
 */
angular.module('bos.crm.manager').controller('ShowManagerCtrl', function (manager, $scope) {
    $scope.manager = manager; //$route.current.locals.manager;
    //refreshInfo($scope.defaultDuration);

//    $http.get('/bos/api/manager/info/' + $scope.manager.id).success(function (data) {
//        $scope.manager.info = data;
//    });

    $scope.$on('$viewContentLoaded', function() {
        Metronic.initAjax(); // initialize core components
    });

    $scope.auditPreset = [
        {fld: 'author.id', op: 'eq', val: $scope.manager.id},
        {fld: 'task.author.id', op: 'eq', val: $scope.manager.id},
        {fld: 'task.responsible.id', op: 'eq', val: $scope.manager.id}
    ];

    $scope.tab = 0;

/*    function refreshInfo(duration) {
        $http.post('/bos/api/manager/info/' + $scope.manager.id, duration).success(function (data) {
            $scope.manager.info = data;
        });
    }*/

/*    $scope.$on("DateChanged", function (event, duration) {
        refreshInfo(duration);
    });*/
});