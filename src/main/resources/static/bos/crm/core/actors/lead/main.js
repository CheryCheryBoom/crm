'use strict';
/**
 * Created by dmytro on 23.08.14.
 */
angular.module('bos.crm.lead', ['bos.common']).config(function ($stateProvider) {

    //files for layout and style, from metronic template
    var commonFiles = [
        '../../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
        '../../../assets/admin/pages/css/profile.css',
        '../../../assets/admin/pages/css/tasks.css',
        '../../../assets/global/plugins/jquery.sparkline.min.js',
        '../../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',

        '../../../assets/admin/pages/scripts/profile.js'//,
        //'/bos/crm/core/actors/common/ContactDirectives.js'
    ];

    $stateProvider
        .state('lead', { //shows lead table
            url: "/lead",
            templateUrl: "/bos/crm/core/actors/lead/views/index.html",
            data: {pageTitle: 'Leads'},
            controller: "LeadListCtrl",
            ncyBreadcrumb: {
                label: 'Leads'
            },
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'bos.crm.lead',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            '/bos/crm/core/actors/lead/js/LeadListCtrl.js'
                        ]
                    });
                }]
            }
        })
        .state('lead.import', {
            url: "/import",
            //abstract:true,
            ncyBreadcrumb: {
                label: 'Lead Import'
            },
            views: {
                "leads-import": {
                    templateUrl: "/bos/crm/upload/views/import.html",
                    controller: "FileUploadCtrl"
                }
            },
            data: {pageTitle: 'Lead Import'},
            onEnter: function(){
                console.log("Lead import")
            },
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'bos.crm.lead',
                        insertBefore: '#ng_load_plugins_before',
                        files: _.union(commonFiles, [
                            '/bos/crm/upload/js/FileInputDirective.js',
                            '/bos/crm/upload/js/FileUploadCtrl.js',
                            '/assets/global/plugins/jquery-file-upload/css/jquery.fileupload.css',
                            '/assets/global/plugins/jquery-file-upload/css/jquery.fileupload-ui.css'
                        ])
                    });
                }]
            }
        })
        .state("lead.show", {
            url: "/{id:int}",
            //abstract:true,
            templateUrl: "/bos/crm/core/actors/lead/views/show/dashboard.html",
            views: {
                "@": {
                    templateUrl: "/bos/crm/core/actors/lead/views/profile.html",
                    controller: "LeadShowCtrl"
                }
            },
            data: {pageTitle: 'Lead Profile'},
            ncyBreadcrumb: {
                label: '{{lead.contact.fullName}}'
            },
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'bos.crm.lead',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: _.union(commonFiles, [
                            '/bos/crm/core/actors/lead/js/LeadShowCtrl.js',
                            '/bos/crm/core/actors/lead/js/LeadEditCtrl.js',
                            '/bos/crm/audit/js/AuditCtrl.js'
                        ])
                    });
                }],
                lead: ['$stateParams', 'Restangular', function ($stateParams, Restangular) {
                    if ($stateParams.id > 0) {
                        return Restangular.one('bos/crud/lead', $stateParams.id).get({projection:"detail"});
                    }
                    else {
                        var lead = Restangular.one('bos/crud/lead');
                        lead.contact = {fullName: '', fields: []};
                        return lead;
                    }
                }]
            }
        })
        .state("lead.show.dashboard", {
            url: '/home',
            templateUrl: "/bos/crm/core/actors/lead/views/profile/dashboard.html"
        })

        .state("lead.show.activity", {
            url: '/activity',
            templateUrl: "/bos/crm/audit/views/list.html",
            controller: 'AuditCtrl',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'bos.crm.audit',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            '/bos/crm/audit/js/AuditCtrl.js'
                        ]
                    });
                }],
                preset: function (lead) {
                    return [
                        {fld: 'lead.id', op: 'eq', val: lead.id},
                        {fld: 'task.lead.id', op: 'eq', val: lead.id},
                        {fld: 'deal.lead.id', op: 'eq', val: lead.id}
                    ];
                }
            }})

        .state("lead.show.edit", {
            abstract: true,
            url: "/edit",
            templateUrl: "/bos/crm/core/actors/lead/views/edit/account.html",
            controller: 'LeadEditCtrl',
            data: {pageTitle: 'Lead Profile Edit'},
            onExit: function(lead){
                lead.contact.fields = _.reject(lead.contact.fields, function (field) {
                    return !field.value;
                });
            }
        })

        .state("lead.show.edit.info", {
            url: "/info",
            templateUrl: "/bos/crm/core/actors/lead/views/edit/info.html"
        })
        .state("lead.show.edit.other", {
            url: "/other",
            templateUrl: "/bos/crm/core/actors/lead/views/edit/other.html"
        });
});

//angular.module('bosDirectives').directive('bosWorkers', function () {
//    return{
//        restrict: 'A',
//        scope: true,
//        templateUrl: '/bos-app/actors/lead/bricks/list.html',
//        controller: 'ListLeadCtrl'
//    }
//});