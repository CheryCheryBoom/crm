/**
 * Created by dmytro on 30.07.14.
 */
angular.module('bos.crm.manager').controller('EditManagerCtrl', function (manager, paging, $state, $modal, $scope, Restangular, $location, $http, $filter /*companyService*/) {

    /*    $scope.tabConfig = {
        tabIndex: 0,
        tabs: [
            {name: 'INFO', icon: 'fa fa-info', template: '/bos-app/actors/manager/bricks/edit/account.html'}
        ]
    };*/


    if (manager) {
        $scope.manager = manager; //$route.current.locals.manager;
        $scope.manager.contact.fields.push(
            /*{type: 'PHONE', description: 'WORKING', value: ""},
            {type: 'EMAIL', description: 'WORKING', value: ""},
            {type: 'SOCIAL', description: 'FACEBOOK', value: ""}*/
        );
    } else {
        $scope.manager = {contact: {fields: []}};
        //todo if ($routeParams.company)
        //    companyService.find($routeParams.company).then(function (data) {
        //        $scope.company.company = data;
        //    });
    }


    $scope.save = function (manager) {
        var operation = $scope.manager.id ? 'update' : 'save';
        //function saveWorker() {


        manager.contact.fields = _.reject(manager.contact.fields, function (field) {
            return !field.value;
        });

        manager.save();

        /*managerService[operation]($scope.manager).then(function (data) {
            $rootScope.managerList[data.id] = data;
            if (operation == 'save') {
                managerService.setRole(data, 'ROLE_MANAGER');
                $rootScope.managerCount++;
            }
        });*/

        //$scope.cancel();
    };

/*    $scope.$watch('manager.contact.fields', function (now, old) {
        var filtered = imageService.getFacebook(now);
        var url = utilService.makeUrl(filtered);
        if (url.length > 0) {
            $http.get(url).success(function (data) {
                if (!$scope.manager.contact.fullName)
                    $scope.manager.contact.fullName = data.name;
                $scope.manager.contact.male = data.gender.toUpperCase() != 'FEMALE';
                $scope.image = url + '/picture?type=large';
            }).error(function (resp) {
                $scope.image = url + '/picture?type=large';
            });
        }
    }, true);*/


    $scope.pass = '';
    $scope.pass2 = '';

    $scope.setPass = function (pass) {
        $http.put('/bos/api/user/update-pass', {pass: pass}).success(function (data) {

        });
    };
});