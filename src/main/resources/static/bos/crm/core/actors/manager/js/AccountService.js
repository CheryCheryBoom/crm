/**
 * Created by dmytro on 24.09.14.
 */
/**
 * Created by dmytro on 27.05.14.
 */
angular.module('bosServices').service('accountService', function ($resource, $q) {
    var resource = $resource('/bos/crud/account/:id', {id: '@id'},
        {
            update: {method: 'PUT'},
            filter: {url: '/bos/filter/mailAccount', method: 'POST'}
        });
    return {
        findAll: function (filter) {
            return resource.filter(filter);
        },
        save: function (account) {
            var deferred = $q.defer();
            resource.save(account,
                function (response) {
                    deferred.resolve(response);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        },
        update: function (account) {
            var deferred = $q.defer();
            resource.update(account,
                function (response) {
                    deferred.resolve(response);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        },
        remove: function (account) {
            var deferred = $q.defer();
            resource.delete(account,
                function (response) {
                    deferred.resolve(response);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        }
    };
});