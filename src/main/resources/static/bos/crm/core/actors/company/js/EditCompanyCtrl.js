/**
 * Created by dmytro on 18.07.14.
 */
angular.module('bos.crm.company').controller('EditCompanyCtrl', function (company, paging, $state, $modal, $scope, Restangular, $location, $http, $filter) {
//        if (!$scope.editOptions) {
//            $scope.editOptions = {hideChildControls: false};
//        }

        if (company) {
            $scope.company = company; //$route.current.locals.company;
            $scope.company.fields.push(
                {type: 'PHONE', description: 'WORKING', value: ""},
                {type: 'EMAIL', description: 'WORKING', value: ""},
                {type: 'SOCIAL', description: 'FACEBOOK', value: ""}
            );
//            if (!$scope.company.address) {
//                $scope.company.address = {comment};
//            }
        } else {
            //if CompanyCtrl owning route, because Lead Ctrl can be parent and have company in scope
            //if (!$scope.editOptions.hideChildControls) {
            $scope.company = {fields: [
                {type: 'PHONE', description: 'WORKING', value: ""},
                {type: 'EMAIL', description: 'WORKING', value: ""},
                {type: 'SOCIAL', description: 'FACEBOOK', value: ""}
            ]};
            //}
        }

        $scope.save = function (company) {
            var operation = $scope.company.id ? 'update' : 'save';

            company.fields = _.reject(company.fields, function (field) {
                return !field.value;
            });

            company.save();

/*            companyService[operation]($scope.company).then(function (data) {
                if ($scope.companyModal)
                    $scope.companyModal.close({company: data});
                else {
                    companyService.show(data.id);
                }
            });*/
        };

        $scope.cancel = function () {
            if ($scope.companyModal)
                $scope.companyModal.dismiss('cancel');
            else $rootScope.cancel();
        };

        $scope.tabConfig = {
            tabIndex: 0,
            tabs: [
                {name: 'INFO', icon: 'fa fa-info', template: '/bos/crm/actors/company/views/edit/account.html'},
                {name: 'REQUISITES', icon: 'fa fa-building', template: '/bos/crm/actors/company/views/edit/requisites.html'}
                // {name: 'STATISTICS', icon: 'fa fa-bar-chart-o', template: '/bos-app/actors/lead/bricks/edit/extra.html'}
            ]
        };
    }
)
;
