/**
 * Created by dmytro on 30.05.14.
 */
angular.module('bos.crm.company').controller('ListCompanyCtrl', function ($scope, $http, paging, $window) {
    paging.init('company', $scope, $scope.companyPreset);
    $scope.reload();

    $scope.report = function (format) {
        $window.location = '/export/lead.' + format;
    };

    $scope.fireFilter = function (text) {
        $scope.filter.composite = [
            {
                "or": [
                    {"fld": "title", "op": "ilike", "val": '%' + text + '%'}
                ]
            }
        ];
        $scope.pager.currentPage = 1;
        $scope.reload();
    };

/*    $scope.$watch('pager.simple', function (now) {
        $scope.fireFilter(now);
    });*/
});


