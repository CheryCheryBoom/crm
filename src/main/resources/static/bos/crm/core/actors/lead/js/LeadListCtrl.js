/**
 * Created by dmytro on 16.07.14.
 */
angular.module('bos.crm.lead').controller('LeadListCtrl', function ($scope, $http, paging, $window) {
    paging.init('lead', $scope, $scope.leadPreset);
    $scope.reload();
    //$scope.table = {};

    $scope.report = function (format) {
        $window.location = '/export/lead.' + format;
    };

    $scope.fireFilter = function (text) {
        $scope.filter.composite = [{
            "cond": "and",
            "filtObj": [{"fld": "contact.fullName", "op": "ilike", "val": '%' + text + '%'}]
        }];
        $scope.pager.currentPage = 1;
        $scope.reload();
    };

    //$scope.$watch('pager.simple', function (now) {
    //    $scope.fireFilter(now);
    //});

    $scope.setPriority = function (lead, priority) {
        lead.priority = priority.value;
        //todo leadService.update(lead);
    };
});