/**
 * Created by dmytro on 16.07.14.
 */
'use strict';
angular.module('bos.crm.lead').controller('LeadEditCtrl', function (lead, paging, $state, $modal, $scope, $location, $http, $filter /*companyService*/) {

//    $scope.editOptions = {
//        hideChildControls: true,
//        editOptions: {companyOption: 'existed'}
//    };
    $scope.company = {fields: []};
    $scope.images = {};

    if (lead) {
        $scope.lead = lead;
        //$scope.editOptions.companyOption = 'existed';

        $scope.lead.contact.fields.push(
            {type: 'PHONE', description: 'WORKING', value: ""},
            {type: 'EMAIL', description: 'WORKING', value: ""},
            {type: 'SOCIAL', description: 'FACEBOOK', value: ""}
        );

        //$scope.lead.contact.fields = _.uniq($scope.lead.contact.fields);

    } else {
        $scope.editOptions.companyOption = 'new';
        $scope.lead = {
            contact: {
                male: true, fields: [
                    {type: 'PHONE', description: 'WORKING', value: ""},
                    {type: 'EMAIL', description: 'WORKING', value: ""},
                    {type: 'SOCIAL', description: 'FACEBOOK', value: ""}
                ]
            }, source: 'MY_OWN', priority: 0
        };
        //todo if ($routeParams.company)
        //    companyService.find($routeParams.company).then(function (data) {
        //        $scope.lead.company = data;
        //    });
    }

//    if ($scope.lead.company)
//        $scope.editOptions.companyOption = 'existed';
//    else
//        $scope.editOptions.companyOption = 'no';

    $scope.save = function (lead) {
        lead.contact.fields = _.reject(lead.contact.fields, function (field) {
            return !field.value;
        });

        lead.save();

        //$scope.cancel();
        //$state.go("lead.show");
    };
    $scope.Existed = function (name) {
        paging.filter('lead',
            [
                {'or': [
                    {op: 'eq', fld: 'contact.fullName', val: name}
                ]}
            ]
        ).then(function (response) {
                $scope.list = response;
            });
    };
    $scope.createCompany = function () {
        $scope.companyModal = $modal.open({
            templateUrl: '/bos-app/actors/common/modal.html',
            controller: 'EditCompanyCtrl',
            backdrop: 'static',
            scope: $scope,
            size: 'lg'//,'sm',
//            resolve: {
//                onSave: function
//            }
        });

        $scope.companyModal.result.then(function (response) {
            $scope.lead.company = response.company;
        });
    };

    //$scope.findCompanyByName = companyService.findAllByName;

//    $scope.tabConfig = {
//        tabIndex: 0,
//        tabs: [
//            {name: 'INFO', icon: 'fa fa-info', template: '/bos-app/actors/lead/bricks/edit/account.html'}
////            {name: 'COMPANY', icon: 'fa fa-building', template: '/bos-app/actors/lead/bricks/edit/company.html'}
//        ]
//    };
});
/*
angular.module('bos.crm.lead').directive('uniqueFullName', function (paging, $q) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, el, attr, ctrl) {
            ctrl.$asyncValidators.uniqueFullName = function (modelValue, viewValue) {
                return paging.filter('lead',
                        [
                            {'or': [
                                {op: 'eq', fld: 'contact.fullName', val: modelValue}
                            ]}
                        ], 'searchWithFields'
                    ).then(function (response) {
                        scope.list = response;
                            if (scope.list.length != 0)
                                return $q.reject('uniqueFullName');
                        });
                };
        }
    };
});*/
