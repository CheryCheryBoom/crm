/**
 * Created by dmytro on 09.06.14.
 */
angular.module('bosServices').service('dealService', function ($resource, $q, bosDate) {
    var resource = $resource('/bos/crud/deal/:id', {id: '@id'}, {
        update: {method: 'PUT'},
        filter: {url: '/bos/filter/deal', method: 'POST'}
    });
    return{
        find: function (dealId) {
            var deferred = $q.defer();
            resource.get({id: dealId},
                function (data) {
                    deferred.resolve(data);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        },
        findAll: function (filter) {
            return resource.filter(filter);
        },
        findAllByNameAndLead: function (name, leadId) {
            var deferred = $q.defer();
            var filter = {
                config: 'search',
                composite: [
                    {"and": [
                        {fld: "title", op: "ilike", val: '%' + name + '%'}
                    ]}
                ]};
            if (leadId) filter.composite[0]["and"].push({fld: "lead.id", op: "eq", val: leadId});
            resource.filter(filter,
                function (data) {
                    deferred.resolve(data.data);
                },
                function (response) {
                    deferred.reject(response);
                }
            )
            ;
            return deferred.promise;
        },
        save: function (deal) {
            deal.orientedDate = bosDate.format(deal.orientedDate);
            var deferred = $q.defer();
            resource.save(deal,
                function (response) {
                    deferred.resolve(response);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        },
        update: function (deal) {
            deal.orientedDate = bosDate.format(deal.orientedDate);
            var deferred = $q.defer();
            resource.update(deal,
                function (response) {
                    deferred.resolve(response);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        }
    };
});