/**
 * Created by dmytro on 22.07.14.
 */
angular.module('bos.crm.deal').controller('EditDealCtrl', function (deal, paging, $state, $modal, $scope, Restangular, $location, $http, $filter) {
    if (!$scope.deal) {
        $scope.deal = {type: 'SALE_SERVICE', status: 'PROPOSAL', orientedDate: new Date(), items: [
            //{measure: 'шт.'}
        ]};

        /*if (lead)
            leadService.find(lead).then(function (data) {
                $scope.deal.lead = data;
            });*/
    } else {
        $scope.deal = deal;
    }
    $scope.save = function (dealForm, deal) {
/*        var operation = deal.id ? 'update' : 'save';
        dealService[operation](deal).then(function (data) {
            $location.url('/show-deal/' + data.id);
        });*/
        deal.items = _.reject(deal.items, function (items) {
            return !items.value;
        });

        deal.save();
    };

    //$scope.findLeadByName = leadService.findAllByName;
});
