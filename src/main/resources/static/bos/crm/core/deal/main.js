'use strict';
/**
 * Created by dmytro on 23.08.14.
 */
angular.module('bos.crm.deal', ['bos.common']).config(function ($stateProvider) {
/*    function resolveDeal($q, $route, dealService) {
        var deferred = $q.defer();
        dealService.find($route.current.pathParams.dealId)
            .then(function (data) {
                deferred.resolve(data);
            });
        return deferred.promise;
    }*/

    var commonFiles = [
        '../../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
        '../../../assets/admin/pages/css/profile.css',
        '../../../assets/admin/pages/css/tasks.css',

        '../../../assets/global/plugins/jquery.sparkline.min.js',
        '../../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',

        '../../../assets/admin/pages/scripts/profile.js'//,
        //'/bos/crm/core/actors/common/ContactDirectives.js'
    ];

    $stateProvider
        .state('deal', { //shows deal table
            url: "/deal",
            templateUrl: "/bos/crm/core/deal/views/index.html",
            data: {pageTitle: 'Deals'},
            controller: "ListDealCtrl",
            ncyBreadcrumb: {
                label: 'Deals'
            },
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'bos.crm.deal',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            '/bos/crm/core/deal/js/ListDealCtrl.js'
                        ]
                    });
                }]
            }
        })
        .state("deal.show", {
            url: "/{id:int}",
            //abstract:true,
            views: {
                "@": {
                    templateUrl: "/bos/crm/core/deal/views/show.html",
                    controller: "ShowDealCtrl"
                }
            },
            data: {pageTitle: 'Deal Profile'},
            ncyBreadcrumb: {
                label: '{{deal.title}}'
            },
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'bos.crm.deal',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: _.union(commonFiles, [
                            '/bos/crm/core/deal/js/ShowDealCtrl.js'
                        ])
                    });
                }],
                deal: ['$stateParams', 'Restangular', function ($stateParams, Restangular) {
                    if ($stateParams.id > 0) {
                        return Restangular.one('bos/crud/deal', $stateParams.id).get();
                    }
                    else {
                        var deal = Restangular.one('bos/crud/deal');
                        deal.item = {items: []};
                        return deal;
                    }
                }]
            }
        })

        .state("deal.show.activity", {
            url: '/activity',
            templateUrl: "/bos/crm/audit/views/list.html",
            controller: 'AuditCtrl',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'bos.crm.audit',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            '/bos/crm/audit/js/AuditCtrl.js'
                        ]
                    });
                }],
                preset: function (deal) {
                    return [
                        {fld: 'deal.id', op: 'eq', val: deal.id},
                        {fld: 'task.deal.id', op: 'eq', val: deal.id}
                    ];
                }
            }})

        .state("deal.edit", {
            url: "/{id:int}/edit",
            views: {
                "@": {
                    templateUrl: "/bos/crm/core/deal/views/edit.html",
                    controller: "EditDealCtrl"
                }
            },
            data: {pageTitle: 'Edit Deal'},
            ncyBreadcrumb: {
                label: '{{deal.title}}'
            },
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'bos.crm.deal',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: _.union(commonFiles, [
                            '/bos/crm/core/deal/js/EditDealCtrl.js'
                        ])
                    });
                }],
                deal: ['$stateParams', 'Restangular', function ($stateParams, Restangular) {
                    return Restangular.one('bos/crud/deal', $stateParams.id).get();
                }]
            }
        });
        // Task Profile Dashboard
        /*.state("task.show.edit", {
         url: "/edit",
         templateUrl: "/bos/crm/core/task/views/edit.html",
         controller: 'EditTaskCtrl',
         data: {pageTitle: 'Task Profile Edit'}
         })*/

/*        //Task Profile Account
        .state("deal.new.edit", {
            abstract: true,
            url: "/new",
            views: {
                "@": {
                    templateUrl: "/bos/crm/core/deal/views/profile.html",
                    controller: 'EditDealCtrl'
                }
            },
            data: {pageTitle: 'Add new Deal'},
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'bos.crm.deal',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: _.union(commonFiles, [
                            '/bos/crm/core/task/js/EditDealCtrl.js'
                        ])
                    });
                }],
                deal: function (Restangular) {
                   var deal = Restangular.one('bos/crud/deal');//{contact:{fields:[]}};
                   deal.contact = {items:[]};
                   return deal;
                }
            }
        })
        // Task Profile Dashboard
        .state("deal.new.edit", {
            url: "/edit",
            templateUrl: "/bos/crm/core/deal/edit/account.html",
            data: {pageTitle: 'Deal Profile Edit'}
        });*/
/*    $routeProvider.
        *//* ----------------- deals ---------------- *//*
        when('/list-deal', {
            templateUrl: '/bos-app/deal/bricks/list.html',
            controller: 'ListDealCtrl'
        }).
        when('/add-deal', {
            templateUrl: '/bos-app/deal/bricks/account.html',
            controller: 'EditDealCtrl'
        }).
        when('/edit-deal/:dealId', {
            templateUrl: '/bos-app/deal/bricks/account.html',
            controller: 'EditDealCtrl',
            resolve: {
                deal: function ($q, $route, dealService) {
                    return resolveDeal($q, $route, dealService);
                }
            }
        }).
        when('/show-deal/:dealId', {
            templateUrl: '/bos-app/deal/bricks/show.html',
            controller: 'ShowDealCtrl',
            resolve: {
                deal: function ($q, $route, dealService) {
                    return resolveDeal($q, $route, dealService);
                }
            }
        }).
        when('/show-invoice/:dealId', {
            templateUrl: '/bos-app/deal/bricks/invoice.html',
            controller: 'ShowDealCtrl',
            resolve: {
                deal: function ($q, $route, dealService) {
                    return resolveDeal($q, $route, dealService);
                }
            }
        });*/
});

/*
angular.module('bosDirectives').directive('bosItems', function () {
    return{
        restrict: 'A',
        templateUrl: '/bos-app/deal/bricks/items.html',
        scope: {items: "="},
        controller: 'FieldsCtrl'
    }
});

angular.module('bosDirectives').directive('bosDeal', function () {
    return{
        restrict: 'A',
        templateUrl: '/bos-app/deal/bricks/show/account.html',
        scope: {deal: "="}
    }
});*/
