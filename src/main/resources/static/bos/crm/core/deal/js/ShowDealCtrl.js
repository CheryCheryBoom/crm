/**
 * Created by dmytro on 30.05.14.
 */
angular.module('bos.crm.deal').controller('ShowDealCtrl', function ($scope, deal) {
    $scope.deal = deal; // $route.current.locals.deal;

    $scope.$on('$viewContentLoaded', function() {
        Metronic.initAjax(); // initialize core components
    });

    $scope.auditPreset = [
        {fld: 'deal.id', op: 'eq', val: $scope.deal.id},
        {fld: 'task.deal.id', op: 'eq', val: $scope.deal.id}
    ];
});