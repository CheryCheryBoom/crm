'use strict';
/**
 * Created by dmytro on 23.08.14.
 */
angular.module('bos.crm.audit', ['bos.common']).config(function ($stateProvider) {
    $stateProvider
        .state("activity", {
            url: '/activity',
            templateUrl: "/bos/crm/audit/views/list.html",
            data: {pageTitle: 'Activities'},
            controller: 'AuditCtrl',
            ncyBreadcrumb: {
                label: 'Activities'
            },
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'bos.crm.audit',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            '/bos/crm/audit/js/AuditCtrl.js'
                        ]
                    });
                }],
                preset: function () {
                    return [];
                }
            }
        })
});