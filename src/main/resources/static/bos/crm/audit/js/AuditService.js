/**
 * Created by dmytro on 12.06.14.
 */
angular.module('bos.common').service('auditService', function ($resource, $q) {
    var resource = $resource('/bos/crud/event/:id', {id: '@id'},
        {
            filter: {url: '/bos/filter/event', method: 'POST'},
            //byLead: {url: '/bos-event/by-lead', params: {leadId: '@leadId', contactId: '@contactId'}, isArray: true},
            byManager: {url: '/bos-event/by-manager', params: {username: '@username'}, isArray: true},
            count: {url: '/bos-event/count', method: 'POST'}
        });
    return {
        find: function (leadId) {
            var deferred = $q.defer();
            resource.get({id: leadId},
                function (data) {
                    deferred.resolve(data);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        },
        findAll: function (filter) {
            return resource.filter(filter);
        },
        save: function (lead) {
            var deferred = $q.defer();
            resource.save(lead,
                function (response) {
                    deferred.resolve(response);
                },
                function (response) {
                    deferred.reject(response);
                });
        },
        update: function (lead) {
            var deferred = $q.defer();
            resource.update(lead,
                function (response) {
                    deferred.resolve(response);
                },
                function (response) {
                    deferred.reject(response);
                });
        },
        findAllByLead: function (lead) {
            //var deferred = $q.defer();
            return resource.filter({'composite':[
                    {'or': [
                        {fld: 'lead.id', op: 'eq', val: lead.id},
                        {fld: 'contact.id', op: 'eq', val: lead.contact.id}
                    ]}
                ]});//,
//                function (data) {
//                    deferred.resolve(data);
//                },
//                function (response) {
//                    deferred.reject(response);
//                });
//            return deferred.promise;
        },
        findAllByManager: function (manager) {
            var deferred = $q.defer();
            resource.byManager({username: manager.email},
                function (data) {
                    deferred.resolve(data);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        }
    };
});