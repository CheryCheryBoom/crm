'use strict';
/**
 * Created by dmytro on 12.06.14.
 */
angular.module('bos.crm.audit').controller('AuditCtrl', function ($rootScope, $scope, Restangular, paging, preset, moment, bosDate) {
    $scope.author = null;
    $scope.type = null;
    $scope.startDate = {fld: 'dateCreated', op: 'ge', val: bosDate.format(moment($rootScope.defaultDuration.start).toDate())};
    $scope.endDate = {fld: 'dateCreated', op: 'le', val: bosDate.format(moment($rootScope.defaultDuration.end).toDate())};

    paging.init('event', $scope, preset);
    $scope.filter.sort = 'dateCreated';
    reload();

    $scope.all = function () {
        $scope.author = null;
        reload();
    };

    $scope.my = function (profile) {
        $scope.author = {fld: 'author.id', op: 'eq', val: profile.id};
        reload();
        console.log($scope.author);
    };

    $scope.allTypes = function () {
        $scope.type = null;
        reload();
    };

    $scope.getByType = function (type) {
        $scope.type = {fld: 'className', op: 'eq', val: type};
        reload();
    };

    $scope.$on('DateChanged', function (event, duration) {
        $scope.startDate = {fld: 'dateCreated', op: 'ge', val: bosDate.format(duration.start)};
        $scope.endDate = {fld: 'dateCreated', op: 'le', val: bosDate.format(duration.end)};
        reload();
    });

    function reload() {
        switch (null){
            case $scope.author: $scope.filter.composite = [
                {'cond': "and", 'filtObj': [ $scope.type, $scope.startDate, $scope.endDate]}
            ];break;
            case $scope.type: $scope.filter.composite = [
                {'cond': "and", 'filtObj': [ $scope.author, $scope.startDate, $scope.endDate]}
            ];break;
            case $scope.type && $scope.author: $scope.filter.composite = [
                {'cond': "and", 'filtObj': [$scope.startDate, $scope.endDate]}
            ];break;
            default : $scope.filter.composite = [
                {'cond': "and", 'filtObj': [$scope.author, $scope.type, $scope.startDate, $scope.endDate]}
            ];break;
        }
        $scope.reload();
    }
});