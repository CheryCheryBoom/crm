/**
 * Created by dmytro on 15.06.14.
 */
angular.module('bos.common').directive('bosFeed', function ($rootScope) {
    return {
        restrict: 'E',
        templateUrl: '/bos/crm/audit/views/feed.html',
        scope: {
            log: '='
        }
    };
});

angular.module('bos.common').directive('bosFeeds', function () {
    return{
        restrict: 'A',
        templateUrl: '/bos/crm/audit/views/list.html',
        scope: true,
        controller: 'AuditCtrl'
    }
});

angular.module('bos.common').filter('bosAuditDescr', function ($rootScope) {
    var i18n = $rootScope.i18n;
    return function getDescr(log) {
        var descr = i18n.actions[log.type].name[log.author.contact.male ? 1 : 2] + ' ';
        var domain = log.class.toLowerCase();

        if (log.type == 'INSERT') {
            descr += i18n.domain[domain].name[2].toLowerCase();
        }
        else if (log.type == 'UPDATE'){
            descr += i18n.domain[domain][log.fieldName][2].toLowerCase() + ' ' + i18n.domain[domain].name[3].toLowerCase();
            }

        return descr;
    };
});


angular.module('bos.common').filter('bosAuditEntity', function () {
    return function getRefDescr(log) {
        switch (log.class) {
            case 'Manager':
            case 'Lead':
                return log.entity.contact.fullName;
            case 'Contact':
                return log.entity.fullName;
            case 'Company':
            case 'Task':
            case 'Deal':
                return log.entity.title;
        }
    };
});

angular.module('bos.common').filter('bosAuditLink', function () {
    return function getRef(log) {
        switch (log.class) {
            case 'Lead':
                return '/show-lead/' + log.entity.id;
            case 'Contact':
                return '/show-contact/' + log.entity.id;
            case 'Task':
                return '/edit-task/' + log.entity.id;
        }
    };
});

angular.module('bos.common').filter('bosAuditField', function () {
    return function getPropDescr(property) {
        switch (property.name) {
            case 'startDate':
            case 'dueDate':
            case 'dateCreated':
            case 'lastUpdated':
                return $filter('date')(Date.parse(property.value), 'dd.MM.yyyy HH:mm');
            case'lead':
                return '№' + property.value.substr(property.value.lastIndexOf(':') + 1);
            default:
                return property.value;
        }
    };
});

/*
angular.module('bos.common').filter('bosChanges', function ($rootScope) {
    var i18n = $rootScope.i18n;
    return function getChangesDescr(log) {
        var descr;
        if(log.oldValue === 'null'){
            descr = i18n.actions["INSERT"].name[0] + log.newValue;
            console.log(log.oldValue);
        } else {
            descr = i18n.actions["UPDATE"].name[0] + 'значення з' + log.oldValue + log.newValue;
        }
        return descr;
    };
});*/
