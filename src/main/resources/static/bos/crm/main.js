'use strict';
var MetronicApp = angular.module("MetronicApp", [
    "ui.router",
    "ui.bootstrap",
    "oc.lazyLoad",
    "ngSanitize"
]);

MetronicApp.config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        // global configs go here
    });
}]);

MetronicApp.factory('settings', ['$rootScope', function($rootScope) {
    // supported languages
    var settings = {
        layout: {
            pageSidebarClosed: false, // sidebar state
            pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
        },
        layoutImgPath: Metronic.getAssetsPath() + 'admin/layout/img/',
        layoutCssPath: Metronic.getAssetsPath() + 'admin/layout/css/'
    };

    $rootScope.settings = settings;

    return settings;
}]);

/***
Layout Partials.
By default the partials are loaded through AngularJS ng-include directive. In case they loaded in server side(e.g: PHP include function) then below partial
initialization can be disabled and Layout.init() should be called on page load complete as explained above.
***/

/* Setup Layout Part - Header */
MetronicApp.controller('HeaderController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initHeader(); // init header
    });
}]);

/* Setup Layout Part - Sidebar */
MetronicApp.controller('SidebarController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initSidebar(); // init sidebar
    });
}]);

/* Setup Layout Part - Theme Panel */
MetronicApp.controller('ThemePanelController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Demo.init(); // init theme panel
    });
}]);

/* Setup Layout Part - Footer */
MetronicApp.controller('FooterController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initFooter(); // init footer
    });
}]);

/* Setup App Main Controller */
MetronicApp.controller('AppController', ['$scope', '$rootScope', function($scope, $rootScope) {
    $scope.$on('$viewContentLoaded', function() {
        Metronic.initComponents(); // init core components
        //Layout.init(); //  Init entire layout(header, footer, sidebar, etc) on page load if the partials included in server side instead of loading with ng-include directive
    });
}]);

/* Init global settings and run the app */
MetronicApp.run(["$rootScope", "settings", "$state", function($rootScope, settings, $state) {
    $rootScope.$state = $state; // state to be accessed from view
}]);

angular.module('bos.common.service', []);

angular.module('bos.common', ['MetronicApp', 'restangular', 'bos.common.service', 'ncy-angular-breadcrumb', 'frapontillo.bootstrap-switch', 'angularMoment', 'ngAnimate'/*, 'textAngular', 'ngIdle', 'angular-loading-bar', 'leaflet-directive'*/])
    .config(function ($breadcrumbProvider, $stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/lead");

        $breadcrumbProvider.setOptions({
            prefixStateName: 'home',
            templateUrl: '/tpl/breadcrumbs.html'
        });


        $stateProvider
            .state('home', {
                url: "/home",
                templateUrl: "/bos/crm/core/actors/lead/views/list.html",
                data: {pageTitle: 'Dashboard'},
                controller: "HomeCtrl",
                ncyBreadcrumb: {
                    label: '<i class="fa fa-home"></i> Home'
                }
            })
            .state('search', {
                url: "/search/{search}",
                templateUrl: "/bos/crm/common/layout/selects/extra_search.html",
                data: {pageTitle: 'Search Results'},
                controller: "SearchCtrl",
                ncyBreadcrumb: {
                    label: '<i class="fa fa-search"></i> Search'
                }
            });
        //$idleProvider.idleDuration(180);
        //$idleProvider.warningDuration(10);
        //$keepaliveProvider.interval(10);
    }).run(function ($rootScope, $http, paginationConfig, $moment) {
        $moment.locale('uk');
        $rootScope.defaultDuration = {start: $moment().subtract('days', 29).valueOf(), end: $moment().valueOf()};

        $http.get('/bos/api/user/me.json').success(function (data) {
            $rootScope.myProfile = data;
        });

          $http.get('/bos/crud/manager').success(function (data) {
                    $rootScope.managerCount = data.length;
        });


        $rootScope.i18n = $http.get('/bos/i18n/ua.json').success(function (data) {
            $rootScope.i18n = data;
            paginationConfig.firstText = data.pager.first;
            paginationConfig.lastText = data.pager.last;
            paginationConfig.previousText = data.pager.previous;
            paginationConfig.nextText = data.pager.next;
        });
    }).constant('$moment', moment);

//main module
angular.module('bos.crm', [
    'bos.crm.lead','bos.crm.audit', 'bos.crm.company', 'bos.crm.manager',
    'bos.crm.task', 'bos.crm.deal',
    'bos.crm.report']);
