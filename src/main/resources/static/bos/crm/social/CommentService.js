/**
 * Created by dmytro on 24.09.14.
 */
/**
 * Created by dmytro on 27.05.14.
 */
angular.module('bosServices').service('commentService', function ($resource, $q) {
    var resource = $resource('/bos/social/comment/:id', {id: '@id'},
        {
            update: {method: 'PUT'},
            filter: {url: '/bos/filter/comment', method: 'POST'}
        });
    return {
        findAll: function (filter) {
            return resource.filter(filter);
        },
        save: function (comment) {
            var deferred = $q.defer();
            resource.save(comment,
                function (response) {
                    deferred.resolve(response);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        },
        update: function (comment) {
            var deferred = $q.defer();
            resource.update(comment,
                function (response) {
                    deferred.resolve(response);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        }
    };
});