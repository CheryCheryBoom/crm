/**
 * Created by dmytro on 18.07.14.
 */
angular.module('bosServices').service('suggestionService', function ($http, toast, $modal) {
    return {
        openModal: function () {
            var modalInstance = $modal.open({
                templateUrl: '/bos-app/suggestions/suggestion.html',
                controller: function ($scope, $modalInstance) {
                    $scope.send = function (subject, desc) {
                        $http.post('/bos/api/user/suggestion', {"subject": subject, "desc": desc}).success(function (data) {
                            toast.success(data);
                        });
                        $modalInstance.close();
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                },
                backdrop: 'static'
                //size: 'lg','sm'
            });
        }
    }
});