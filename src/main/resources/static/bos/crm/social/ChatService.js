/**
 * Created by dmytro on 24.09.14.
 */
/**
 * Created by dmytro on 27.05.14.
 */
angular.module('bosServices').service('chatService', function ($resource, $q) {
    var resource = $resource('/bos/social/chat/:id', {id: '@id'},
        {
            update: {method: 'PUT'},
            filter: {url: '/bos/filter/lead', method: 'POST'}
        });
    return {
        findAll: function (filter) {
            return resource.filter(filter);
        },
        save: function (message) {
            var deferred = $q.defer();
            resource.save(message,
                function (response) {
                    deferred.resolve(response);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        },
        update: function (message) {
            var deferred = $q.defer();
            resource.update(message,
                function (response) {
                    deferred.resolve(response);
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        }
    };
});