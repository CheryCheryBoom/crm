/**
 * Created by dmytro on 24.09.14.
 */
angular.module('bosSocial', []);
angular.module('bosSocial').directive('bosComments', function (toast) {
    return {
        restrict: 'A',
        transclude: true,
        scope: true,
        templateUrl: '/bos-app/social/comments.html',
        controller: function ($scope, commentService, paging) {
            paging.init(commentService, $scope, $scope.commentPreset);
            $scope.filter.sort = 'dateCreated';
            $scope.reload();

            $scope.postComment = function (comment) {
                comment.task = {id: $scope.task.id};
                commentService.save(comment).then(function () {
                    $scope.reload();
                    $scope.comment = {};
                });
            };

            $scope.$on('CommentArrived', function (event, comment) {
                if (comment.task.id == $scope.task.id) {
                    toast.show(comment.text);
                    $scope.reload();
                }
            });
        }
    };
});