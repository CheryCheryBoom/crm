/**
 * Created by dmytro on 20.08.14.
 */
'use strict';

angular.module('bos.crm').run(function($rootScope, $document){
    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
        $document[0].title = toState.data.pageTitle;
    });
});

//angular.module('bosApp').run(['managerService', 'alarmService', '$idle', 'bosSocket', '$rootScope', '$http', 'imageService', '$timeout', 'bosDate', '$window', '$modal', '$q', 'paginationConfig', 'moment', 'amMoment', 'toast', 'suggestionService', function (managerService, alarmService, $idle, bosSocket, $rootScope, $http, imageService, $timeout, bosDate, $window, $modal, $q, paginationConfig, moment, amMoment, toast, suggestionService) {
//    $rootScope.defaultDuration = {start: moment().subtract('days', 29).valueOf(), end: moment().valueOf()};
//
//    managerService.myProfile().then(function (data) {
//        $rootScope.profile = data;
//
//
//        moment.locale('uk');
//        amMoment.changeLanguage('uk');
//
//
//        alarmService.byManager(data.id).then(function (alarms) {
//            angular.forEach(alarms, function (value, key) {
//                $rootScope.alarms = alarms;
//                var diff = moment(value.date).valueOf() - moment().valueOf();
//                if (diff > 0)
//                    $timeout(function () {
//                        toast.info("Task - " + value.task.id)
//                    }, diff);
//            });
//        });
//
//        $rootScope.like = function () {
//            suggestionService.openModal();
//        };
//
//        bosSocket.init();
//
//        $rootScope.i18n = $http.get('/bos-app/util/i18n/ua.json').success(function (data) {
//            $rootScope.i18n = data;
//            paginationConfig.firstText = data.pager.first;
//            paginationConfig.lastText = data.pager.last;
//            paginationConfig.previousText = data.pager.previous;
//            paginationConfig.nextText = data.pager.next;
//        });
//
//
//        $http.get('/bos/crud/manager').success(function (data) {
//            $rootScope.managerList = {};
//            angular.forEach(data, function (value, key) {
//                this[value.id] = value;
//            }, $rootScope.managerList);
//
//            $rootScope.managerCount = data.length;
//        });
//
//        $rootScope.go = function (uri) {
//            $location.url(uri);
//        };
//
//        $rootScope.cancel = function () {
//            $window.history.back();
//        };
//
//        $rootScope.isFormValid = function (form) {
//            return form.$valid;
//        };
//
//        function closeModals() {
//            if ($rootScope.warning) {
//                $rootScope.warning.close();
//                $rootScope.warning = null;
//            }
//        }
//
//        $rootScope.$on('$idleStart', function () {
//            closeModals();
//
//            $rootScope.warning = $modal.open({
//                template: '<div class="modal-header"><h3>Ви довгий час не взаємодіяли з системою.</h3></div>' +
//                    '<div ng-idle-countdown="countdown" ng-init="countdown=10" class="modal-body">' +
//                    '<progressbar max="10" value="countdown" animate="false" class="progress-striped progress-bar-warning active">' +
//                    'Вас викине через {{countdown}} секунд.</progressbar></div>',
//                windowClass: 'modal-danger'
//            });
//        });
//
//        $rootScope.$on('$idleEnd', function () {
//            closeModals();
//        });
//
//        $rootScope.$on('$idleTimeout', function () {
//            closeModals();
//            $window.location = '/logout';
//        });
//
//        $rootScope.$on("$locationChangeStart", function (event, newUrl) {
//            $http.get('/bos/api/user/logged').success(function (data, status) {
//                //alert('Сесія жива-' + status);
//            }).error(function (data, status) {
//                event.preventDefault();
//                alert('Ваша сесія скінчилася. Зайдіть ще раз');
//                $window.location = newUrl;
//            });
//        });
//
//        $idle.watch();
//    });
//
//}]);