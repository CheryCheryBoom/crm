/**
 * Created by dmytro on 01.06.14.
 */
angular.module('bos.common.service').service('bosGoogleService', function ($http) {
    return {
        getLocations: function (val) {
            return $http.get('http://maps.googleapis.com/maps/api/geocode/json', {
                params: {
                    address: val,
                    sensor: false
                }
            }).then(function (res) {
                //var addresses = [];
                //angular.forEach(res.data.results, function (item) {
                //    addresses.push(item.formatted_address);
                //});
                //return addresses;
                return res.data.results;
            });
        }
    }
});
angular.module('bos.common.service').service('bosFacebookService', function () {
    return {
        makeUrl: function (facebook) {
            var theUrl = 'http://graph.facebook.com';
            if (!angular.isUndefined(facebook) && facebook !== null) {
                theUrl = facebook;
                var pos = theUrl.indexOf('facebook.com');
                if (pos != -1) {
                    var posToDelete = theUrl.indexOf('fref');
                    if (posToDelete != -1) {
                        theUrl = theUrl.substring(0, posToDelete - 1);
                    }
                    theUrl = theUrl.replace('profile.php?id=', '');
                    theUrl = 'http://graph.' + theUrl.substring(pos, theUrl.length);
                }
            }
            return theUrl;
        }
    }
});

angular.module('bos.common.service').service('bosImageService', function (bosFacebookService, $filter) {
    return{
        getFacebook: function (fields) {

            var filtered = $filter('filter')(fields, {type: 'SOCIAL', description: 'FACEBOOK'});
            if (!angular.isUndefined(filtered) && filtered !== null && filtered.length > 0)
                return filtered[0].value;
            else return null;
        },
        makeImage: function (facebook) {
            return bosFacebookService.makeUrl(facebook) + '/picture?type=small';
        },
        makeBigImage: function (facebook) {
            return bosFacebookService.makeUrl(facebook) + '/picture?type=large';
        }
    }
});

angular.module('bos.common.service').service('bosDate', function ($filter, moment) {
    return{
        parse: function (time) {
            return moment(time).toDate();
            //return moment(date, 'DD.MM.yyyy HH:mm:ss').toDate();
        },
        parsetoMoment: function (time) {
            return moment(time);
        },
        format: function (date) {
            return $filter('date')(date, 'dd.MM.yyyy HH:mm:ss');
        },
        toMoment: function (date) {
            return moment(date, 'DD.MM.yyyy HH:mm:ss');
        },
        now: function (date) {
            return moment();
        }
    }
});

angular.module('bos.common.service').service('i18nService', function ($rootScope) {
    return{
        getLocale: function () {
            return $rootScope.i18n;
        }
    }
});