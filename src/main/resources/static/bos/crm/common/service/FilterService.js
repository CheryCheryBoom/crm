/**
 * Created by dmytro on 14.09.14.
 */
angular.module('bos.common.service').factory('paging', function ($http, $q) {
    var filterPath = 'bos/crud/';

    return {
        init: function (resource, scope, preset) {
            scope.filter = {
                max: 5,
                offset: 0,
                sort: 'id',
                order: 'asc',
                mainCondition: 'and',
                composite: []
            };

            scope.pager = {
                simple: '',
                currentPage: 1,
                pagerSize: 5,
                fireSort: function (filter, sort) {
                    filter.sort = sort;
                    if (filter.order == 'asc')
                        filter.order = 'desc';
                    else
                        filter.order = 'asc';
                    scope.reload();
                }
            };

            scope.reload = function () {
                if (angular.isDefined(preset) && preset) {
                    scope.filter.composite.push({'cond': 'or', 'filtObj': preset});
                }

                scope.filter.offset = (scope.pager.currentPage - 1) * scope.filter.max;
                $http.post('bos/filter/' + resource, scope.filter).success(function (response) {
                    var oldIds = _.pluck(scope.data,'id');
                    var newIds = _.pluck(response.data,'id');
                    if(!_.isEqual(oldIds,newIds)){
                        scope.count = response.count;
                        scope.data = response.data;
                    }
                });
            };
        },

        filter: function (resource, customFilter, config) {
            config = config || 'seacrh';
            var deferred = $q.defer();
            var filter = {
                config: config,
                composite: customFilter
            };
            $http.post('bos/filter/' + resource, filter)
                .success(function (data) {
                    deferred.resolve(data.data);
                }).error(function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        }
    };
});