/**
 * Created by dmytro on 31.07.14.
 */
angular.module('bosServices').service('bosSocket', function ($rootScope, toast) {
    //var sock = new SockJS('http://localhost:8585/socket');
    //var sock = new vertx.EventBus('http://bos-demo.jelastic.neohost.net:8585/bos/socket');
    var sock = new vertx.EventBus('http://localhost:8585/bos/socket');

    return{
        init: function () {
//            sock.onmessage = function (e) {
//                console.log('From server <-', e.data);
//            };
            sock.onopen = function () {
                console.log('Socket open');

                sock.registerHandler($rootScope.profile.id + '', function (message) {
                    var data = JSON.parse(message);
                    if (data['class'].indexOf('Message') > -1) {
                        $rootScope.$broadcast('MessageArrived', data);
                        toast.info($rootScope.managerList[data.author.id].contact.fullName, data.text);
                    } else if (data['class'].indexOf('Comment') > -1)
                        $rootScope.$broadcast('CommentArrived', data);
                    else if (data['class'].indexOf('Alarm') > -1)
                        $rootScope.$broadcast('AlarmArrived', data);
                });
            };

            sock.onclose = function () {
                console.log('Socket closed');
            }
        },
        send: function (message, address) {
            if (sock.readyState == WebSocket.OPEN) {
                console.log("To -> " + address + '. -> ' + message);
                sock.send(address, message);
            } else {
                console.log("The socket is not open.");
            }
        }
    }
})
;