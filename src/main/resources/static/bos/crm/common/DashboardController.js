/**
 * Created by dmytro on 16.07.14.
 */

angular.module('bosApp').controller('DashboardCtrl', function ($scope, $http) {
    $http.get('/bos/api/board').success(function (data) {
        $scope.dashboard = data;
    });
});