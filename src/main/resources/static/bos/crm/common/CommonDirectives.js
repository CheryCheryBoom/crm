/**
 * Created by dmytro on 15.07.14.
 */
angular.module('bos.common').directive('bosFields', function ($rootScope) {
    return {
        restrict: 'A',
        templateUrl: '/bos/crm/common/directives/fields.html',
        transclude: true,
        scope: {
            fields: '=',
            fieldType: '@'
        },
        controller: function ($scope) {
            $scope.i18n = $rootScope.i18n;
            $scope.fieldDescriptions = $rootScope.i18n.fieldTypes[$scope.fieldType].descriptions;

            $scope.remove = function (field) {
                $scope.fields.splice($scope.fields.indexOf(field), 1);
            };

            $scope.add = function (fieldType) {
                $scope.fields.push({type: fieldType, description: fieldType == 'SOCIAL' ? 'WEBSITE' : 'WORKING', value: ''});
            }
        }
    };
});

angular.module('bos.common').directive('bosCalendar', function (bosDate, $rootScope) {
    //FILTER AND FUNCTIONAL TRANSFER INTO SEPARATE FILE !!!!!!!!!!!!!!!!!!!!!
    return{
        restrict: 'A',
        scope: {},
        link: function (scope, element) {
            var i18n = $rootScope.i18n;
            scope.calendar = [];

            angular.forEach(data, function (value, key) {
                this.push({
                    title: getTitle(value.title),
                    url: '#/task/' + value.id,
                    start: bosDate.parse(value.startDate),
                    end: bosDate.parse(value.dueDate),
                    backgroundColor: i18n.taskStatuses[value.status].color //Metronic.getBrandColor('blue')
                })
            }, scope.calendar);

            element.fullCalendar({
                lang: 'uk',
                header: {
                    right: 'title, prev, next',
                    center: '',
                    left: ''
                },
                events: scope.calendar,
                timeFormat: 'H(:mm)',
                buttonText: {
                    today: 'Сьогодні',
                    month: 'Місяць',
                    week: 'Тиждень',
                    day: 'День'
                }
            });

            function getTitle(title) {
                var shortTitle;

                if (title.length <= 20) {
                    shortTitle = title;
                }
                else {
                    shortTitle = title.substr(0, 18) + '...'
                }

                return shortTitle;
            }
        }
    }
});

/**
 * Created by dmytro on 22.07.14.
 */
angular.module('bos.common').directive('bosRequisites', function () {
    return {
        restrict: 'A',
        templateUrl: '/bos-app/actors/common/requisites.html',
        scope: {
            items: '='
        },
        controller: 'FieldsCtrl'
    }
});

angular.module('bos.common').directive('bosAccounts', function () {
    return {
        restrict: 'A',
        templateUrl: '/bos-app/actors/common/accounts.html',
        scope: true,
        controller: function ($scope, accountService, paging) {
            paging.init(accountService, $scope, $scope.accountPreset);
            $scope.filter.sort = 'username';
            $scope.filter.max = 100;
            $scope.reload();

            $scope.add = function (account) {
                account.manager = {id: $scope.manager.id};
                if (account)
                    accountService.save(account).then(function () {
                        $scope.account = {};
                        $scope.reload();
                    });
            };

            $scope.remove = function (account) {
                accountService.remove(account).then(function () {
                    $scope.reload();
                });
            };
        }
    }
});

angular.module('bos.common').controller('FieldsCtrl', function ($scope, $rootScope) {
    $scope.i18n = $rootScope.i18n;

    if (angular.isUndefined($scope.items) || !$scope.items) {
        $scope.items = [];
    }

    $scope.addItem = function () {
        $scope.items.push({});
    };

    $scope.removeItem = function (index) {
        $scope.items.splice(index, 1);
    };
});

angular.module('bos.common').directive('bosAva', function (bosImageService, bosFacebookService, $http) {
    return {
        restrict: 'A',
        replace: true,
        template: '<img ng-src="{{profileImage}}" alt="{{contact.fullName}}" class="{{class}}" height="{{h}}" width="{{w}}"/>',
        scope: {
            contact: '=',
            h: '=',
            w: '=',
            class: '@'
        },
        controller: function ($scope) {
            $scope.facebookLink = bosImageService.getFacebook($scope.contact.fields);
            //scope.facebookSmallImage = bosImageService.makeImage(scope.facebookLink);

            if ($scope.facebookLink)
                $scope.profileImage = bosImageService.makeBigImage($scope.facebookLink);
            else
                $scope.profileImage = $scope.contact.male ? '/assets/images/user_male.png' : '/assets/images/user_female.png';

            $scope.$watch('contact.male', function (now) {
                if (!$scope.facebookLink)
                    $scope.profileImage = $scope.contact.male ? '/assets/images/user_male.png' : '/assets/images/user_female.png';
            });


            $scope.$watch('contact.fields', function (now, old) {
                var filtered = bosImageService.getFacebook(now);
                var url = bosFacebookService.makeUrl(filtered);
                if (url.length > 0) {
                    $http.get(url).success(function (data) {
                        if (!$scope.contact.fullName)
                            $scope.contact.fullName = data.name;
                        if (data.gender){
                            $scope.contact.male = data.gender.toUpperCase() != 'FEMALE';
                        } else {
                            $scope.contact.male = true;
                        }
                        $scope.image = url + '/picture?type=large';
                    }).error(function (resp) {
                        $scope.image = url + '/picture?type=large';
                    });
                }
            }, true);
        }
    }
});

angular.module('bos.common').directive('bosContactInfo', function ($rootScope) {
    return {
        restrict: 'A',
        templateUrl: '/bos/crm/core/actors/common/contactInfo.html',
        scope: {
            contact: "="
        },
        link: function (scope, element, attrs, tabsCtrl) {
            scope.i18n = $rootScope.i18n;
            if (angular.isDefined(attrs.isCompany))
                scope.isCompany = true;
        }
    }
});

angular.module('bos.common').directive('bosLeads', function ($rootScope) {
    return {
        restrict: 'A',
        templateUrl: '/bos-app/actors/lead/bricks/list.html',
        scope: {
            company: "="
        },
        controller: 'ListLeadCtrl'
    }
});

angular.module('bos.common').directive('bosAddress', function ($rootScope, $http) {
    return {
        restrict: 'A',
        templateUrl: '/bos/crm/core/actors/common/address.html',
        scope: {
            address: '='
        },
        controller: function ($scope) {
            function message(address) {
                return address.locality + ', ' + address.street + ', ' + address.streetNumber;
            }

            $scope.hasMap = function (address) {
                return address && !angular.isUndefined(address.latitude) && !angular.isUndefined(address.longitude);
            };

            if (!$scope.address)
                $scope.address = {};

            $scope.i18n = $rootScope.i18n;

            $scope.location = {
                lat: 50.450031,
                lng: 30.52420499999999,
                zoom: 16,
                actual: false
            };

            $scope.markers = {
                mainMarker: {
                    lat: 50.450031,
                    lng: 30.52420499999999,
                    message: "Київ. Майдан Незалежності",
                    focus: false,
                    draggable: true
                }
            };

            if ($scope.hasMap($scope.address)) {
                $scope.location.lat = $scope.markers.mainMarker.lat = $scope.address.latitude;
                $scope.location.lng = $scope.markers.mainMarker.lng = $scope.address.longitude;
                $scope.markers.mainMarker.message = message($scope.address);
                $scope.location.actual = true;
            }

            $scope.getLocations = function (val) {
                $http.get('http://maps.googleapis.com/maps/api/geocode/json', {
                    params: {
                        address: val,
                        sensor: false
                    }
                }).then(function (res) {
                    //var addresses = [];
                    //angular.forEach(res.data.results, function (item) {
                    //    addresses.push(item.formatted_address);
                    //});
                    //return addresses;
                    return res.data.results;
                });
            };

            $scope.selectAddress = function (item, model, label) {
                if (!angular.isObject($scope.address)) {
                    $scope.address = {comment: $scope.address};
                }
                if (!$scope.address.comment)
                    $scope.address.comment = '';

                $scope.address.latitude = $scope.location.lat = $scope.markers.mainMarker.lat = item.geometry.location.lat;
                $scope.address.longitude = $scope.location.lng = $scope.markers.mainMarker.lng = item.geometry.location.lng;
                $scope.location.actual = true;

                angular.forEach(item.address_components, function (value) {
                    //console.log(value);
                    switch (value.types[0]) {
                        case 'street_number':
                            $scope.address.streetNumber = value.long_name;
                            break;
                        case 'route':
                            $scope.address.street = value.long_name;
                            break;
                        case 'sublocality_level_1':
                            $scope.address.sublocality = value.long_name;
                            break;
                        case 'locality':
                            $scope.address.locality = value.long_name;
                            break;
                        case 'administrative_area_level_2':
                            $scope.address.area = value.long_name;
                            break;
                        case 'administrative_area_level_1':
                            $scope.address.region = value.long_name;
                            break;
                        case 'country':
                            $scope.address.country = value.long_name;
                            break;
                        default:
                            $scope.address.comment += value.long_name + '\n';
                            break;
                    }
                });
                $scope.address.textual = $scope.markers.mainMarker.message = message($scope.address);
//            defaults: {
//                scrollWheelZoom: false
//            }
            };
        }
    };
});

angular.module('bos.common').directive('bosPager', function () {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: '/bos/crm/common/directives/pager.html'
    }
});

angular.module('bos.common').directive('tile', function () {
    return {
        restrict: 'A',
        transclude: true,
        scope: {number: '@', title: '@'},
        template: '<div ng-transclude class="tile-body"></div>' +
            '<div class="tile-object"><div class="name">{{title}}</div>' +
            '<div class="number">{{number}}</div></div>'
    };
});

angular.module('bos.common').directive('tileChecked', function () {
    return {
        restrict: 'A',
        scope: {number: '@', title: '@'},
        template: '<div class="corner"></div><div class="check"></div>' +
            '<div class="tile-body"><i class="fa fa-cogs"></i></div>' +
            '<div class="tile-object"><div class="name">Settings</div></div>'
    }
});

angular.module('bos.common').directive('wideTile', function () {
    return {
        restrict: 'A',
        transclude: true,
        scope: {number: '@', title: '@', linkDesc: '@', link: '@'},
        template: '<div ng-transclude class="visual"></div>' +
            '<div class="details"><div class="number">{{number}}</div><div class="desc">{{title}}</div>' +
            '</div><a class="more" ng-href="{{link}}">{{linkDesc}}<i class="m-icon-swapright m-icon-white"></i></a>'
    }
});
angular.module('bos.common').directive('sorter', function () {
    return {
        restrict: 'A',
        transclude: true,
        // replace: true,
        scope: {
            sort: '@',
            filter: '=',
            paging: "="
        },
        template: '<a href="#" ng-click="paging.fireSort(filter, sort)"><span ng-transclude></span></a>' +
            '<i class="fa fa-sort-asc font-blue pull-right" ng-show="filter.order==' + "'asc'" + ' && filter.sort == sort"></i>' +
            '<i class="fa fa-sort-desc font-blue pull-right" ng-show="filter.order==' + "'desc'" + ' && filter.sort == sort"></i>'
    };
});

angular.module('bos.common').directive('ionSlider', function () {
    return {
        restrict: 'A',
        link: function (scope, element) {
            var slider = element.ionRangeSlider({
                min: -1,
                max: 7,
                from: 0,
                type: 'single',
                step: 1,
                //values:['Чорний','Сірий','Білий','Світло-Сірі','Сині','Зелені','Помаранчеві','Червоні','Екстра'],
                //postfix: "°",
                prettify: true,
                hasGrid: true,
                onChange: function (obj) {
                    scope.entity.priority = obj.fromNumber;
                    scope.$apply();
                }
            });
            scope.$watch('entity.priority', function (newVal) {
                    slider.ionRangeSlider('update', {
                        from: newVal
                    });
                }
            );
        }
    }
});
angular.module('bos.common').directive('knob', function () {
    return {
        restrict: 'A',
        link: function (scope, element) {
            element.knob({
                'min': -1,
                'max': 7,
                'width': '100',
                'angleArc': 240,
                'angleOffset': 240,
                //'dynamicDraw': true,
                'thickness': 0.2,
                'tickColorizeValues': true,
                'skin': 'tron'
            });
        }
    };
});

angular.module('bos.common').directive('bosScroll', function () {
    return {
        restrict: 'A',
        link: function (scope, element) {
            element.slimScroll(
                {
                    //                   width: '300px',
                    height: '300px',
                    //                   size: '10px',
//                    position: 'left',
//                    color: '#ffcc00',
                    alwaysVisible: false
//                    distance: '20px',
//                    start: $('#child_image_element'),
//                    railVisible: true,
//                    railColor: '#222',
//                    railOpacity: 0.3,
//                    wheelStep: 10,
                    //                   allowPageScroll: true
//                    disableFadeOut: true
                }
            );
        }
    };
});

angular.module('bos.common').directive('uniqueFullName', function (paging, $q) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, el, attr, ctrl) {
            ctrl.$asyncValidators.uniqueFullName = function (modelValue, viewValue) {
                return paging.filter(scope.accountData.name,
                    [
                        {'or': [
                            {op: 'eq', fld: scope.accountData.field, val: modelValue}
                        ]}
                    ], 'searchWithFields'
                ).then(function (response) {
                        scope.list = response;
                        if (scope.list.length != 0)
                            return $q.reject('uniqueFullName');
                    });
            };
        }
    };
});

angular.module('bos.common').directive('bosNameValidation', function ($parse) {
    return {
        restrict: 'E',
        scope: true,
        templateUrl: '/bos/crm/common/directives/fullName.html',
        link: function (scope, el, attr, ctrl) {
            scope.accountData = scope.$eval(attr.account);
            scope.actor = $parse(scope.accountData.name + '.' + scope.accountData.field)(scope);
            scope.titleHelp = $parse(scope.accountData.titleHelp)(scope);
        }
    }
});

angular.module('bos.common').directive('bosScroller', function () {
    return {
        restrict: 'A',
        link: function (scope, el, attr, ctrl) {
            //$(el).each(function() {
//                if (el.attr("data-initialized")) {
//                    return; // exit
//                }

            var height;

            if (el.attr("data-height")) {
                height = el.attr("data-height");
            } else {
                height = el.css('height');
            }

            el.slimScroll({
                allowPageScroll: false, // allow page scroll when the element scroll is ended
                size: '7px',
                color: (el.attr("data-handle-color") ? el.attr("data-handle-color") : '#bbb'),
                wrapperClass: (el.attr("data-wrapper-class") ? el.attr("data-wrapper-class") : 'slimScrollDiv'),
                railColor: (el.attr("data-rail-color") ? el.attr("data-rail-color") : '#eaeaea'),
                position: /*isRTL ? 'left' :*/ 'right',
                height: height,
                alwaysVisible: (el.attr("data-always-visible") == "1" ? true : false),
                railVisible: (el.attr("data-rail-visible") == "1" ? true : false),
                disableFadeOut: true
            });

            //el.attr("data-initialized", "1");
            //}
        }
    }
});

angular.module('bos.common').directive('bosNotEmpty', function () {
    return {
        restrict: 'E',
        scope: true,
        template: '<div ng-show="dataForm.$submitted || dataForm[field].$dirty">'+
            '<div class="help-block" ng-show="dataForm[field].$error.required">{{message}}</div>'+
            '<div class="help-block" ng-show="dataForm[field].$error.min || dataForm[field].$error.max">This value is out of range</div>'+
            '<div class="help-block" ng-show="dataForm[field].$invalid && dataForm[field].$error.min && !dataForm[field].$error.required && !dataForm[field].$error.max">Please enter a valid value</div>'+
            '</div>',
        link: function (scope, el, attr, ctrl) {
            scope.message=attr.message;
            scope.field = attr.field;
        }
    }
});

angular.module('bos.common').directive('bosSchedule', function () {
    return {
        restrict: 'E',
        templateUrl: 'assets/bos/crm/common/schedule/list.html',
        scope: {
            log: '='
        }
/*        link: function (scope, el, attr, ctrl) {
           scope.list = attr.list;
        }*/
    }
});