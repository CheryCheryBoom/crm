/**
 * Created by Den4ik on 04.02.2015.
 */
angular.module('bos.crm.task').controller('ScheduleCtrl', function ($scope, paging, $rootScope) {
    paging.init('task', $scope, $scope.taskPreset);
    $scope.filter.sort = 'dateCreated';
    reload();

    $scope.setPriority = function (task, priority) {
        task.priority = priority.value;
        //todo taskService.update(task);
    };

    function reload() {
        $scope.filter.composite = [
            {'cond': "or", 'filtObj': [{op: 'eq', fld: 'responsible.id', val: $rootScope.myProfile.id}]}
        ];
        $scope.reload();

    }

});