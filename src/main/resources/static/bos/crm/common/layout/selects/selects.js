/**
 * Created by dmytro on 28.09.14.
 */
angular.module('bos.common').directive('bosEntitySelect', function ($http) {
    return{
        restrict: 'A',
        scope: {entity: "=", bosFindFn: "&", bosOnSelect: "&", onBosMatch: "&"},
        link: function (scope, element, attrs) {
            element.select2({
                placeholder: "Обрати...",
                minimumInputLength: 2,
                query: function (query) {
                    var array = [];
                    scope.bosFindFn({term: query.term}).then(function (data) {
                        query.callback({results: data});
                    });
                },
                initSelection: function (element, callback) {
                    callback({id: scope.entity.id, text: scope.entity.text});
                },
//                formatNoMatches: function (term) {
//                    if (scope.noMatchFunc)
//                        return "<a href='#' ng-click='noMatchFunc({term:" + "term})'>Додати <i class='fa fa-plus'></i></a>";
//                    else
//                        return "Відповідностей не знайдено"
//                },
                //formatResult: movieFormatResult, // omitted for brevity, see the source of this page
                //formatSelection: movieFormatSelection, // omitted for brevity, see the source of this page
                dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
                escapeMarkup: function (m) {
                    return m;
                }
            }).on("change", function (e) {
                scope.entity = e.added;

                if (scope.bosOnSelect)
                    scope.bosOnSelect({entity: e.added});
                //log("change " + JSON.stringify({val: e.val, added: e.added, removed: e.removed}));
            });
//            if (scope.entity && scope.entity.id)
//                element.select2("val", {id: scope.entity.id, text: scope.entity.title});

            scope.$watch('entity', function (now) {
                element.select2("val", now);
            });
        }
    }
});

angular.module('bos.common').directive('bosFullSearch', function ($http, $location) {
    return{
        restrict: 'A',
        templateUrl: '/bos/crm/common/layout/selects/full-search.html',
        scope: true,
        controller: function ($scope, $state) {


            $scope.lookUp = function (text) {
                return $http.post('/bos/search', {text: text}).then(function (response) {
                    return response.data
                    $scope.asyncSelected = '';
                });
            };

            $scope.selectSearch = function ($item, $model, $label) {
                $location.url('/' + $item.class.toLowerCase() + '/' + $item.id);
                $scope.asyncSelected = '';
            };

            $scope.runSearch = function (searchValue) {
                $scope.search = searchValue.typeahead.$viewValue;
                if ($scope.search != '')
                    $state.go('search', {search: $scope.search});
                $scope.asyncSelected = '';
            };
        }
    }
});

/*angular.module('bos.common').directive('bosSearchList', function ($http, $location) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope,element,ctrl,attr){
        $element.typeahead(
        { hint: true,
          highlight: true,
          minLength: 1
        },
        { name: 'name',
          displayKey: 'value',
          source: search.ttAdapter(),
          *//* templates: {suggestion: Handlebars.compile(['<h3>{{result[0].value}}</h3>'].join(''))}*//*
          });
        },
        controller: function($scope, $element) {
        var results = [];
           $scope.lookUp = function () {
                    $http.post('/bos/search', {text: $scope.searchResult}).then(function (response) {
                        results = [{value: response.data[0].title}];


                                        var search = new Bloodhound({
                                                   datumTokenizer:  function(d) { return Bloodhound.tokenizers.whitespace(d.value); },
                                                   queryTokenizer: Bloodhound.tokenizers.whitespace,
                                                   local: results
                                                 });
                                        search.initialize();
                        *//*                results.push({ value: 'Info' });*//*
                        console.log(results);

                    });
            };
        }
    }
});*/


angular.module('bos.common').controller('SearchCtrl', function ($stateParams, $state, $http, $location, $scope) {
    var searchList = [
        [],
        [],
        [],
        [],
        []
    ];
    $http.post('/bos/search', {text: $stateParams.search}).then(function (response) {
        $scope.search = response.data;
        console.log($scope.search);
        angular.forEach($scope.search, function (item) {
            switch (item.class) {
                case 'Lead' :
                    searchList[0].push(item);
                    break;
                case 'Company' :
                    searchList[1].push(item);
                    break;
                case 'Manager' :
                    searchList[2].push(item);
                    break;
                case 'Deal' :
                    searchList[3].push(item);
                    break;
                case 'Task' :
                    searchList[4].push(item);
                    break;

            }
        });
        $scope.searchList = searchList;
    });
});


