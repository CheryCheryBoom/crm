/**
 * Created by dmytro on 20.08.14.
 */
angular.module('bos.common').directive('bosProfileDropdown', function () {
    return{
        restrict: 'A',
        transclude: true,
        templateUrl: '/bos/crm/common/layout/profileDropdown.html'
    }
});

angular.module('bos.common').directive('bosChatToggler', function () {
    return{
        restrict: 'A',
        transclude: true,
        template: '<a href="javascript:;" ng-click="showQuickBar()" class="dropdown-toggle"><i class="icon-logout"></i></a>',
        controller: function ($scope) {
            $scope.showQuickBar = function () {
                angular.element('body').toggleClass('page-quick-sidebar-open');
            };
        }
    }
});

angular.module('bos.common').directive('breadcrumbs', function ($rootScope, $filter) {
    return{
        restrict: 'A',
        templateUrl: '/bos/crm/common/layout/breadcrumbs.html',
       // scope: true,
        controller: function ($scope, $state, $location, $document) {
            function crumb(crumb) {
                $scope.layout.breadcrumbs.push(crumb);
            }

            function toolbar(tool) {
                $scope.layout.toolbar.push(tool);
            }

            function addButton(link) {
                toolbar({name: 'Додати', icon: 'fa fa-plus', class: 'btn blue', link: link});
            }

            function editButton(link) {
                toolbar({name: 'Змінити', icon: 'fa fa-pencil', class: 'btn blue', link: link});
            }

            function currentCrumb(icon) {
                crumb({name: $scope.title, icon: icon, link: $location.path()});
            }

            $scope.$on("$stateChangeSuccess", function (event, current, previous) {
                    $scope.layout = {
                        dateRanger: false,
                        exporter: false,
//                        breadcrumbs: [
//                            {name: 'Головна', icon: 'fa fa-home', link: '/dashboard'}
//                        ],
                        toolbar: []
                    };

                    switch (current.controller) {
                        /* ------------------------- Company ------------------------------------------*/
                        case 'ListCompanyCtrl':
                            currentCrumb('fa fa-building');
                            addButton('/add-company');
                            break;
                        case 'ShowCompanyCtrl':
                            editButton('/edit-company/' + current.locals.company.id);
                        case 'EditCompanyCtrl':
                            if (current.locals.company)
                                title(current.locals.company.title);
                            crumb({name: 'Компанії', icon: 'fa fa-building', link: '/list-company'});
                            currentCrumb('fa fa-building-o');
                            break;
                        /* ------------------------- Lead --------------------------------------------*/
                        case 'ListLeadCtrl':
                            //$scope.layout.exporter = true;
                            currentCrumb('fa fa-users');
                            addButton('/add-lead');
                            break;
                        case 'ShowLeadCtrl':
                            editButton('/edit-lead/' + current.locals.lead.id);
                        case 'EditLeadCtrl':
                            if (current.locals.lead)
                                title(current.locals.lead.contact.fullName);
                            crumb({name: 'Ліди', icon: 'fa fa-users', link: '/list-lead'});
                            currentCrumb('fa fa-user');
                            break;
                        /* ------------------------- Manager --------------------------------------------*/
                        case 'ListManagerCtrl':
                            currentCrumb('fa fa-users');
                            addButton('/add-manager');
                            break;
                        case 'ShowManagerCtrl':
                            $scope.layout.dateRanger = true;
                            editButton('/edit-manager/' + current.locals.manager.id);
                        case 'EditManagerCtrl':
                            if (current.locals.manager)
                                title(current.locals.manager.contact.fullName);
                            crumb({name: 'Колеги', icon: 'fa fa-users', link: '/list-manager'});
                            currentCrumb('fa fa-user');
                            break;

                        case 'AuditCtrl':
                            $scope.layout.dateRanger = true;
                            break;
                        case 'ReportCtrl':
                            $scope.layout.dateRanger = true;
                            currentCrumb('icon-bar-chart');
                            break;
                        case 'ListDealCtrl':
                            currentCrumb('fa fa-pencil-o');
                            addButton('/add-deal');
                            break;
                        case 'ShowDealCtrl':
                            if (current.locals.deal) {
                                title($rootScope.i18n.DEAL + ' № ' + current.locals.deal.id);
                                editButton('/edit-deal/' + current.locals.deal.id);
                                $scope.subtitle = ' від ' + $filter('date')(current.locals.deal.dateCreated, 'dd MMMM yyyy HH:mm');
                            }
                            currentCrumb('fa fa-shopping-cart');
                            break;
                        case 'ListTaskCtrl':
                            currentCrumb('fa fa-check');
                            addButton('/add-task?type=TASK');
                            break;
                    }
                }
            );
        }
    }
});
