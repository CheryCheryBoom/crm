/**
 * Created by dmytro on 30.09.14.
 */
angular.module('bosDirectives').directive('bosBlockUi', function ($rootScope, moment) {
    return{
        restrict: 'A',
        link: function (scope, element) {
            scope.blockUI = function (options) {
                blockUI(element, options);
            };

            scope.unblockUI = function () {
                unblockUI(element);
            };
        },
        controller: function ($scope) {
            $scope.$on("$locationChangeStart", function (event, current, previous) {
                $scope.blockUI({});
            });

            $scope.$on("$viewContentLoaded", function (event, current, previous) {
                $scope.unblockUI();
            });
        }
    };
    function blockUI(target, options) {
        options = $.extend(true, {}, options);
        var html = '<div class="loading-message loading-message-boxed">' +
            '<img src="/images/loading-spinner-grey.gif" align="">' +
            '<span>&nbsp;&nbsp; Завантаження...</span></div>';

        if (target) { // element blocking
            blockTarget(target, html, options);
        } else { // page blocking
            blockPage(html, options);
        }
    }

    function blockPage(message, options) {
        $.blockUI({
            message: message,
            baseZ: options.zIndex ? options.zIndex : 1000,
            css: {
                border: '0',
                padding: '0',
                backgroundColor: 'none'
            },
            overlayCSS: {
                backgroundColor: options.overlayColor ? options.overlayColor : '#000',
                opacity: options.boxed ? 0.05 : 0.1,
                cursor: 'wait'
            }
        });
    }

    function blockTarget(target, message, options) {
        //var el = $(options.target);
        if (target.height() <= ($(window).height())) {
            options.cenrerY = true;
        }
        target.block({
            message: message,
            baseZ: options.zIndex ? options.zIndex : 1000,
            centerY: options.cenrerY !== undefined ? options.cenrerY : false,
            css: {
                top: '10%',
                border: '0',
                padding: '0',
                backgroundColor: 'none'
            },
            overlayCSS: {
                backgroundColor: options.overlayColor ? options.overlayColor : '#000',
                opacity: options.boxed ? 0.05 : 0.1,
                cursor: 'wait'
            }
        });
    }

// wrMetronicer function to  un-block element(finish loading)
    function unblockUI(target) {
        if (target) {
            target.unblock({
                onUnblock: function () {
                    target.css('position', '');
                    target.css('zoom', '');
                }
            });
        } else {
            $.unblockUI();
        }
    }
});