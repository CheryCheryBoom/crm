/**
 * Created by dmytro on 20.08.14.
 */
angular.module('bosDirectives').directive('quickBar', function ($http) {
    return{
        restrict: 'A',
        templateUrl: '/bos-app/util/layout/quickBar.html',
        scope: true,
        link: function (scope, element) {
            //var messageBar = element.find(".page-quick-sidebar-chat-user-messages");
//            scope.refresh = function () {
//                var res = element.find(".page-quick-sidebar-chat-user-messages").slimscroll({
//                    destroy: true
//                });
//
//                var res2 = element.find(".page-quick-sidebar-chat-user-messages").slimscroll({
//                    height:300,
//                    alwaysVisible: false
//                });
//
//                console.log(res);
//                console.log(res2);
//
//
//                //                   width: '300px',
//                //                   height: '500px',
//                //                   size: '10px',
////                    position: 'left',
////                    color: '#ffcc00',
////                        alwaysVisible: false
////                    distance: '20px',
////                    start: $('#child_image_element'),
////                    railVisible: true,
////                    railColor: '#222',
////                    railOpacity: 0.3,
////                    wheelStep: 10,
//                //                   allowPageScroll: true
////                    disableFadeOut: true
//
//            }
        },
        controller: function ($scope) {
            $scope.quickTab = 1;
            $scope.chatOpened = false;

            $scope.openChat = function (target) {
                $scope.target = target;
                var filter = {
                    composite: [
                        {'and': [
                            {fld: 'author.id', op: 'eq', val: $scope.profile.id},
                            {fld: 'target.id', op: 'eq', val: target.id}
                        ]},
                        {'and': [
                            {fld: 'target.id', op: 'eq', val: $scope.profile.id},
                            {fld: 'author.id', op: 'eq', val: target.id}
                        ]}
                    ]
                };
                $http.post('/bos/filter/message', filter).success(function (data) {
                    $scope.messages = data.data;
                    $scope.chatOpened = true;
                });
                // $scope.refresh();
            };

            $scope.closeChat = function () {
                $scope.chatOpened = false;
            };

            $scope.send = function (message) {
                console.log($scope);
                message.target = {id: $scope.target.id};
                $http.post('/bos/social/chat', message).success(function (data) {
                    $scope.messages.push(data);
                    message.text = '';
                });
            };

            $scope.$on('MessageArrived', function (event, data) {
                console.log('Message Arrived');
                if (data.author.id != $scope.profile.id) {
                    $scope.messages.push(data);
                    $scope.$apply();
                }
            });
        }
    }
});