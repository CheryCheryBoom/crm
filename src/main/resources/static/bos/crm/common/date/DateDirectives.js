/**
 * Created by dmytro on 01.08.14.
 */
angular.module('bos.common').directive('bosPicker', function () {
    return {
        restrict: 'A',
        scope: {
            date: '='
        },
        link: function (scope, element, attrs, ngModelCtrl) {

            scope.update = function (date) {
                scope.date.setYear(date.getFullYear());
                scope.date.setMonth(date.getMonth());
                scope.date.setDate(date.getDate());

                scope.$apply();
            };

            var picker = element.datepicker({
                language: 'ua',
                format: 'dd.mm.yyyy',
                autoclose: true,
                orientation: "top left",
                todayHighlight: true,
                forceParse: true,
                keyboardNavigation: false,
                daysOfWeekDisabled: '6,0'
//                beforeShowDay: function (date){
//                    if (date.getMonth() == (new Date()).getMonth())
//                        switch (date.getDate()){
//                            case 4:
//                                return {
//                                    tooltip: 'Example tooltip',
//                                    classes: 'active'
//                                };
//                            case 8:
//                                return false;
//                            case 12:
//                                return "green";
//                        }
//                }
            }).on('changeDate', function (ev) {
//                ngModelCtrl.$setViewValue(ev.date);
//                scope.$apply();
                scope.update(ev.date);
            });
        }
    }
});

angular.module('bos.common').directive('bosTime', function () {
    return {
        restrict: 'A',
        scope: {
            date: '='
        },
        link: function (scope, element) {
            element.clockface();
            element.on('pick.clockface', function (ev, data) {
                console.log(data);
                if (data.hour != null)
                    scope.date.setHours(data.hour);
                else scope.date.setHours(0);

                if (data.minute != null)
                    scope.date.setMinutes(data.minute);
                else scope.date.setMinutes(0);

                scope.$apply();
            });
        }
    }
});

angular.module('bos.common').directive('bosDatePicker', function () {
    return {
        restrict: 'A',
        scope: {
            date: '='
        },
        templateUrl: '/bos/crm/common/date/picker.html'
    }
});

angular.module('bos.common').directive('bosReminder', function () {
    return {
        restrict: 'A',
        scope: {
            remind: '=',
            date: '='
        },
        templateUrl: '/bos-app/task/bricks/edit/reminder.html',
        link: function (scope, element) {
            scope.remindFor = function (minutes) {
                scope.remind = new Date(scope.date.getTime() - minutes * 60000);
                scope.openPicker = false;
            };

            scope.$watch('date', function (now, old) {
                console.log(now);
                if (scope.remind)
                    scope.remind = new Date(now.getTime() - (old.getTime() - scope.remind.getTime()))
                console.log(remind);
            });
        }
    }
});

angular.module('bos.common').directive('bosDuration', function () {
    return{
        restrict: 'A',
        template: '<div class="col-md-5"><input type="number" ng-model="showDuration" ng-change="changeDuration()" class="form-control"></div>' +
            '<div class="col-md-4"><select class="col-md-4 form-control" ng-model="durationUnit">' +
            '<option ng-repeat="unit in durationUnits" value="{{unit.name}}" ng-selected="unit.name==durationUnit">{{unit.ua}}</option></select></div>',
        scope: {
            start: '=',
            end: '='
        },
        link: function (scope, element) {
            scope.durationUnits = {
                minute: {name: 'minute', value: 60 * 1000, ua: 'хвилин'},
                hour: {name: 'hour', value: 60 * 60 * 1000, ua: 'годин'},
                day: {name: 'day', value: 24 * 60 * 60 * 1000, ua: 'днів'},
                week: {name: 'week', value: 7 * 24 * 60 * 60 * 1000, ua: 'тижнів'},
                month: {name: 'month', value: 30 * 24 * 60 * 60 * 1000, ua: 'місяців'},
                year: {name: 'year', value: 365 * 24 * 60 * 60 * 1000, ua: 'років'}
            };

            scope.durationUnit = scope.durationUnits.minute.name;

            scope.reloadDuration = function () {
                scope.millis = scope.end.getTime() - scope.start.getTime();
                scope.showDuration = Math.round(scope.millis / scope.durationUnits[scope.durationUnit].value);
            };

            scope.changeDuration = function () {
                scope.end = new Date(scope.start.getTime() + scope.showDuration * scope.durationUnits[scope.durationUnit].value);
            };

            scope.reloadDuration();

            scope.$watch('start', function (newVal, oldVal) {
                scope.reloadDuration();
            });

            scope.$watch('end', function (newVal, oldVal) {
                scope.reloadDuration();
            });

            scope.$watch('durationUnit', function (newVal, oldVal) {
                scope.reloadDuration();
            });
        }
    }
});

angular.module('bos.common').directive('dateRangePicker', function ($filter, $rootScope, moment) {
    return{
        restrict: 'A',
        link: function (scope, element) {
            element.daterangepicker({
                    opens: 'left',
                    startDate: moment($rootScope.defaultDuration.start),
                    endDate: moment($rootScope.defaultDuration.end),
                    minDate: '01/01/2012',
                    maxDate: '12/31/2015',
                    dateLimit: {
                        days: 60
                    },
                    showDropdowns: false,
                    showWeekNumbers: true,
//                    timePicker: true,
//                    timePickerIncrement: 5,
//                    timePicker12Hour: false,
                    ranges: {
                        'Сьогодні': [moment(), moment()],
                        'Вчора': [moment().subtract('days', 1), moment().subtract('days', 1)],
                        'Останні 7 Днів': [moment().subtract('days', 6), moment()],
                        //'Цей Тиждень': [$moment().startOf('week'), $moment().endOf('week')],
                        'Останні 30 Днів': [moment().subtract('days', 29), moment()],
                        'Цей Місяць': [moment().startOf('month'), moment().endOf('month')],
                        'Останній Місяць': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                    },
                    buttonClasses: ['btn btn-sm'],
                    applyClass: ' blue-dark',
                    cancelClass: 'default',
                    format: 'DD.MM.YYYY',
                    separator: ' до ',
                    locale: $rootScope.timeLocale
                },
                function (start, end) {
                    $rootScope.$broadcast('DateChanged', {start: start.valueOf(), end: end.valueOf()});
                    element.children('span').html(
                            $filter('date')(start.toDate(), 'dd MMMM yyyy') + ' - ' +
                            $filter('date')(end.toDate(), 'dd MMMM yyyy'));
                }
            );
            scope.$on("$stateChangeSuccess", function (event, current, previous) {
                element.data('daterangepicker').setStartDate(moment($rootScope.defaultDuration.start));
                element.data('daterangepicker').setEndDate(moment($rootScope.defaultDuration.end));
                element.children('span').html(
                        $filter('date')($rootScope.defaultDuration.start, 'dd MMMM yyyy') + ' - ' +
                        $filter('date')($rootScope.defaultDuration.end, 'dd MMMM yyyy'));
                element.show();
            });
        }
    }
});

angular.module('bos.common').directive('bosDateDiff', function (moment) {
    return{
        restrict: 'A',
        replace: true,
        transclude: true,
        scope: {
            from: '=',
            to: '='
        },
        template: '<span>{{result}}</span>',
        controller: function ($scope) {
            $scope.result = moment($scope.to).from(moment($scope.from), true);
        }
    }
});