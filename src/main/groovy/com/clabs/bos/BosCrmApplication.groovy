package com.clabs.bos

import com.clabs.bos.crm.actors.company.Company
import com.clabs.bos.crm.actors.company.CompanyRepository
import com.clabs.bos.crm.actors.contact.Contact
import com.clabs.bos.crm.actors.contact.bricks.Address
import com.clabs.bos.crm.actors.contact.bricks.Field
import com.clabs.bos.crm.actors.lead.Lead
import com.clabs.bos.crm.actors.lead.LeadRepository
import com.clabs.bos.crm.actors.manager.Manager
import com.clabs.bos.crm.actors.manager.ManagerRepository
import com.clabs.bos.crm.sales.deal.Deal
import com.clabs.bos.crm.sales.deal.DealRepository
import com.clabs.bos.crm.tasks.Participation
import com.clabs.bos.crm.tasks.task.Task
import com.clabs.bos.crm.tasks.task.TaskRepository
import com.clabs.bos.crm.util.auth.User
import com.clabs.bos.crm.util.auth.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.context.web.SpringBootServletInitializer

import javax.annotation.PostConstruct

import static com.clabs.bos.crm.actors.contact.bricks.Field.Type.*

@SpringBootApplication
class BosCrmApplication extends SpringBootServletInitializer {

    @Autowired
    CompanyRepository companyRepository
    @Autowired
    ManagerRepository managerRepository
    @Autowired
    LeadRepository leadRepository
    @Autowired
    DealRepository dealRepository
    @Autowired
    TaskRepository taskRepository
    @Autowired
    UserRepository userRepository

    @PostConstruct
    private void someInits() {
        //Role bosRole = new Role(authority: Role.ROLE_BOS).save()
        //Role managerRole = new Role(authority: Role.ROLE_MANAGER).save()

        Company firstCompany = new Company(title: 'ТОВ Сі Лабз Україна', service: 'J')
        companyRepository.save(firstCompany)
        //firstCompany.addToRequisites(new Requisites(name: 'Основний', accountNumber: '1234567890', accountDetails: 'Приватбанк Київ', bankNumber: '234567', stateId: '0987654321').save())

        Contact bosContact = new Contact(fullName: 'Зонов Дмитро Павлович', male: true)
        bosContact.fields << new Field(type: 'EMAIL', description: 'MAIN', value: 'd.zonov@ukr.net')
        bosContact.fields << new Field(type: 'PHONE', value: '0961140233')
        bosContact.fields << new Field(type: 'SOCIAL', description: 'FACEBOOK', value: 'facebook.com/dmytrozzz')

        Manager bosWorker = new Manager(company: firstCompany, position: 'Генеральний директор', contact: bosContact)
        //new MailAccount(manager: bosWorker, username: 'krepka.klepka@gmail.com', password: 'oprotvereziynyk123', host: 'smtp.googlemail.com', port: 465, protocol: MailAccount.Protocol.SSL)
        managerRepository.save(bosWorker)

        //User bos = new User(username: 'krepka.klepka@gmail.com', password: 'bos123', profile: bosWorker).save()
        // UserRole.create(bos, bosRole)

        Manager manager3 = new Manager(company: firstCompany, position: 'Керівник відділу продажу', contact: new Contact(fullName: 'Анастасія Баюр', fields: Field.make([['nbayur@ukr.net', EMAIL], ['https://www.facebook.com/nbayur', SOCIAL]])), author: bosWorker)
        //User managerUser3 = new User(username: 'nbayur@ukr.net', password: 'poseidon', profile: manager3).save()
        // UserRole.create(managerUser3, managerRole)

        //new Message(text: 'Чекаю тебе в суботу', author: bosWorker, target: manager3).save()
        managerRepository.save(manager3)

        Manager managerWorker = new Manager(company: firstCompany, position: 'Раб', contact: new Contact(fullName: 'Лазарев Денис Сергійович',
                fields: Field.make([['cherry.lazarev@gmail.com', EMAIL], ['0961140233', PHONE], ['https://www.facebook.com/denys.lazarev?fref=ts', SOCIAL]])
        ))
        User admin = new User(name: 'admin', password: 'admin', manager: managerWorker)
        userRepository.save(admin)
        managerRepository.save(managerWorker)


//
//        User manager = new User(username: 'cherry.lazarev@gmail.com', password: 'poseidon123', profile: managerWorker).save()
//        UserRole.create(manager, managerRole)

        Manager managerWorker2 = new Manager(company: firstCompany, position: 'Менеджер з маркетингу', contact: new Contact(fullName: 'Ботар Олександр Вікторович',
                fields: Field.make([['a.botar@ukr.net', EMAIL], ['0961140233', PHONE], ['www.facebook.com/alexandr.botar', SOCIAL]])
        ))
//        User manager2 = new User(username: 'a.botar@ukr.net', password: 'poseidon123', profile: managerWorker2).save()
//        UserRole.create(manager2, managerRole)

        managerRepository.save(managerWorker2)

        def onty = new Lead(company: new Company(title: 'КП Хуст Енерго', service: 'U'), position: "Головний інжинир",
                contact: new Contact(fullName: 'Онтий', male: true,
                        fields: Field.make([['krepka.klepka@gmail.com', EMAIL], ['068000000', PHONE]])),
                author: managerWorker, priority: 7)
        def tanya = new Lead(contact: new Contact(fullName: 'Таня Цапюк', fields: Field.make([['https://www.facebook.com/profile.php?id=100001916911834', SOCIAL]])), author: managerWorker2, priority: 4)
        def artur = new Lead(company: new Company(title: 'ArtPool.com.ua', service: 'F'), contact: new Contact(fullName: 'Артур Баюр', fields: Field.make([['https://www.facebook.com/profile.php?id=100001949062415&fref=ts', SOCIAL]])), author: bosWorker, priority: 4)
        def mykola = new Lead(contact: new Contact(fullName: 'Микола Дем’яненко', fields: Field.make([['https://www.facebook.com/profile.php?id=100005044291591', SOCIAL]])), author: bosWorker, priority: 2)
        def sasha = new Lead(contact: new Contact(fullName: 'Саша Дем’яненко', fields: Field.make([['https://www.facebook.com/profile.php?id=1348042291', SOCIAL]])), author: bosWorker, priority: 1)
        def poroh = new Lead(contact: new Contact(fullName: 'Петро Порошенко', fields: Field.make([['https://www.facebook.com/petroporoshenko?fref=ts', SOCIAL]])), author: bosWorker, priority: 5)
        def arsen = new Lead(contact: new Contact(fullName: 'Арсеній Яценюк', fields: Field.make([['https://www.facebook.com/yatsenyuk.arseniy?fref=ts', SOCIAL]])), author: bosWorker, priority: 5)
        def obama = new Lead(contact: new Contact(fullName: 'Барак Обама', fields: Field.make([['https://www.facebook.com/barackobama?fref=ts&rf=105503206150390', SOCIAL]])), author: bosWorker, priority: 5)
        def ruslan = new Lead(contact: new Contact(fullName: 'Тунік Руслан', fields: Field.make([['https://www.facebook.com/tunikruslan', SOCIAL]])), author: bosWorker, priority: 5)

        onty.company = new Company(title: "КП ХустЕнерго", description: "Контролює електроенергію Хуста", service: "Agro")
        onty.company.address = new Address(country: "Ukraine", region: "Zakarpatska", locality: "Khust", street: "Heroiv Nebesnoi Sotni")

        leadRepository.save([onty, tanya, artur, mykola, sasha, poroh, arsen, obama, ruslan])

        def task1 = new Task(title: 'Дзвінок в Електросіть. Ознайомити з послугами', startDate: new Date(), dueDate: new Date(System.currentTimeMillis() + 500000),
                lead: onty, responsible: managerWorker, author: managerWorker2, type: Task.Type.CALL)

        def deal = new Deal(title: 'Договір на поставку цукру', sum: 4000, /*currency: 'UAH',*/ lead: onty)

        def task = new Task(title: 'Дзвінок партнерам', startDate: new Date(System.currentTimeMillis() - 1000000), dueDate: new Date(System.currentTimeMillis() - 500000),
                lead: onty, responsible: bosWorker, author: bosWorker, type: Task.Type.CALL, deal: deal, status: Task.Status.SUCCESS, result: 'Дзвінок успішно проведено')

        def task2 = new Task(title: 'Електросіть. Нагадати про договір', startDate: new Date(), dueDate: new Date(System.currentTimeMillis() - 500000),
                lead: onty, responsible: bosWorker, author: bosWorker, type: Task.Type.CALL)

        def task3 = new Task(title: 'Руслан. Запропонувати поставки кави.', startDate: new Date(), dueDate: new Date(System.currentTimeMillis() + 500000),
                lead: ruslan, responsible: bosWorker, author: bosWorker, type: Task.Type.CALL)

        def deal1 = new Deal(title: 'Поставка кави', sum: 9000, /*currency: 'UAH',*/ lead: ruslan, status: Deal.Status.PROPOSAL.name())

        def deal2 = new Deal(title: 'Поставка квасу', sum: 10000, /*currency: 'UAH',*/ lead: ruslan, status: Deal.Status.INVOICE.name())


        def deal3 = new Deal(title: 'Поставка солі', sum: 2500, /*currency: 'UAH',*/ lead: onty, status: Deal.Status.CONTRACTING.name())

        def deal4 = new Deal(title: 'Обслуговування субмарин', sum: 40000, /*currency: 'UAH',*/ lead: artur, status: Deal.Status.SUCCESS.name())

        def deal5 = new Deal(title: 'Поставка субмарин', sum: 400000, /*currency: 'UAH',*/ lead: artur, status: Deal.Status.MONEY_IN_THE_BANK.name())


        def afterMonth = new Date().with {
            ++month
            it
        }
        def project = new Task(title: 'Виставка в ТРЦ DreamTown',
                description: 'Конференція + виставка всіх розробників програмного забезпечення',
                responsible: managerWorker, author: bosWorker, startDate: new Date(),
                dueDate: afterMonth,
                type: Task.Type.PROJECT)

        def subtask = new Task(title: 'Підготувати стенд на виставку',
                description: 'Стенд для виставки зі зразками нашої продукції',
                responsible: managerWorker, author: bosWorker, startDate: new Date(),
                dueDate: afterMonth,
                type: Task.Type.TASK, supertask: project)

        def subtask2 = new Task(title: 'Підготувати виступ на конференції',
                description: 'Презентація нашої продукції та приклади вдалого використання',
                responsible: managerWorker2, author: bosWorker, startDate: new Date(),
                dueDate: afterMonth,
                type: Task.Type.TASK, supertask: project)

        dealRepository.save([deal, deal1, deal2, deal3, deal4, deal5])
        taskRepository.save([task, task1, task2, task3, project, subtask, subtask2])

        new Participation(participant: manager3, task: project, watcher: true)

    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        application.sources(BosCrmApplication)
    }

    static void main(String[] args) {
        SpringApplication.run BosCrmApplication, args
    }

}
