package com.clabs.bos.infrastructure.filtering

import org.hibernate.validator.constraints.NotEmpty

import javax.validation.constraints.Min

/**
 * Created by dmytro on 19.12.14.
 * Example of json to send: 
 *{"max":10,"offset":10, "sort":"title","order":"asc","config":"search","mainCondition":"or","composite":[{"cond":"and","filtObj":[{"fld":"subject","op":"ilike","val":"%ololo%"}]}]}*/
class FilterRequest {
    public static final String DETAIL = "detail"

    @Min(1l)
    int max

    @Min(0l)
    int offset

    String sort

    String order
    /*
    Use to define needed response configs (Json Named Config)
     */
    String config
    String mainCondition
    List<Filter> composite

    def getPaging() {
        [max: max, offset: offset]
    }

    String getConfig() {
        config ?: DETAIL
    }

    String getSort() {
        sort ?: "id"
    }

    String getOrder() {
        order ?: "asc"
    }

    def getPagingAndSorting() {
        [max: max, offset: offset, sort: sort, order: order]
    }

    String toString() {
        return [composite: composite].toString()
    }
}

class Filter {
    @NotEmpty
    String cond
    List<FilterItem> filtObj

    String getCondition() {
        cond
    }

    List<FilterItem> getFilterItems() {
        filtObj
    }

    String toString() {
        return [condition: cond, objects: filtObj].toString()
    }
}

class FilterItem {
    @NotEmpty
    String fld

    @NotEmpty
    String op

    @NotEmpty
    def val

    String getFieldName() {
        fld
    }

    String getOperation() {
        op
    }

    def getValue() {
        if (val instanceof Integer || "$fld".endsWith(".id"))
            val as Long
        else if (val instanceof String && "$fld".endsWith("dateCreated"))
            return "date"//todo MarshalersRegistar.dateFormat.parse(val)
        else /*if (val instanceof String)*/
            return val as String
    }

    String toString() {
        return [fieldName: fld, operation: op, value: val].toString()
    }
}
