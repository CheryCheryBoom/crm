package com.clabs.bos.infrastructure.filtering

import com.clabs.bos.config.RestConfig
import com.clabs.bos.crm.actors.lead.Lead
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.rest.core.config.RepositoryRestConfiguration
import org.springframework.data.rest.core.projection.ProjectionDefinitions
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*

import javax.validation.Valid

/**
 * Created by dima on 25.03.15.
 */
@Controller
class FilterController {

    @Autowired
    FilterService filterService



    @RequestMapping(value = "/bos/filter/{domain}", method = RequestMethod.POST)
    @ResponseBody
    public Object filter(
            @Valid @RequestBody FilterRequest filterRequest, @PathVariable String domain, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            new ResponseEntity(bindingResult, HttpStatus.BAD_REQUEST)
        } else {
            filterService.partyStarted(domain, filterRequest)
        }
    }
}
