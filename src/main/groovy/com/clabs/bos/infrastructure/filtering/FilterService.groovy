package com.clabs.bos.infrastructure.filtering

import com.clabs.bos.crm.actors.lead.Lead
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.projection.ProjectionFactory
import org.springframework.data.projection.SpelAwareProxyProjectionFactory
import org.springframework.data.rest.core.config.RepositoryRestConfiguration
import org.springframework.stereotype.Service

import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.persistence.criteria.*
import javax.persistence.metamodel.EntityType

/**
 * Created by dima on 25.03.15.
 */
@Service
class FilterService {
    static ordinary = ~/^(?:equal|notEqual|gt|ge|lt|le|like)$/
    static order = ~/^(?:asc|desc)$/

    @PersistenceContext
    EntityManager em

    @Autowired
    private RepositoryRestConfiguration repoRestConfig

    private static final ProjectionFactory projectionFactory = new SpelAwareProxyProjectionFactory()


    def partyStarted(String domain, FilterRequest filterRequest) {
        println filterRequest.properties
        Class clazz = findDomainClassByName(domain) //todo if not found

        CriteriaBuilder cb = em.criteriaBuilder
        CriteriaQuery dataQuery = cb.createQuery(clazz)
        CriteriaQuery<Long> countQuery = cb.createQuery(Long.class);
        Root dataRoot = dataQuery.from(clazz)
        Root countRoot = countQuery.from(clazz)

        Order sorting = cb."$filterRequest.order"(extractField(filterRequest.sort, dataRoot))

        CriteriaQuery select = dataQuery.select(dataRoot).orderBy(sorting)
        CriteriaQuery countSelect = countQuery.select(cb.count(countRoot));

        if (filterRequest?.composite) {
            select.where(buildPredicateChain(filterRequest, dataRoot, cb))
            countSelect.where(buildPredicateChain(filterRequest, countRoot, cb))
        }

        List data = em.createQuery(dataQuery).setMaxResults(filterRequest.max).setFirstResult(filterRequest.offset).resultList
        Long count = em.createQuery(countQuery).getSingleResult()

        Class projection = repoRestConfig.projectionConfiguration().getProjectionType(clazz, filterRequest.config)

        [data : projection ? data.collect { projectionFactory.createProjection(projection, it) } : data,
         count: count]
    }

    private Class findDomainClassByName(String domain) {
        em.metamodel.entities.find { EntityType et ->
            et.name == domain.capitalize()
        }.javaType
    }

    private static Predicate buildPredicateChain(FilterRequest filterRequest, Root root, CriteriaBuilder cb) {
        def mainPredicates = filterRequest.composite.collect { Filter filter ->
            def subPredicates = filter.filterItems.collect { FilterItem filterItem ->
                switch (filterItem?.operation) {
                    case "ilike":
                        return cb.like(cb.lower(extractField(filterItem.fieldName, root)), filterItem.value.toString().toLowerCase())
                    case ordinary:
                        return cb."$filterItem.operation"(extractField(filterItem.fieldName, root), filterItem.value)
                    default:
                        println "Not yet implemented!!!"
                        return []
                }
            }
            cb."$filter.condition"(subPredicates as Predicate[])
        }
        cb."$filterRequest.mainCondition"(mainPredicates as Predicate[])
    }

    private static Path extractField(String fieldName, Root root) {
        if (fieldName.contains(".")) {
            def fields = fieldName.split("\\.")
            Join joinChain = fields[0..fields.length - 2].inject(root) { From from, String field ->
                from.join(field, JoinType.LEFT)
            }
            joinChain.get(fields[-1])
        } else {
            root.get(fieldName)
        }
    }
}
