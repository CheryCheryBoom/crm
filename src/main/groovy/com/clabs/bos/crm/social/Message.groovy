package com.clabs.bos.crm.social

import com.clabs.bos.crm.actors.manager.Manager
import org.hibernate.validator.constraints.NotEmpty
import org.springframework.data.annotation.CreatedDate

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.validation.constraints.NotNull

/**
 * Created by dmytro on 15.08.14.
 */
@Entity
class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;
    def transient userService

    @ManyToOne
    @NotNull
    Manager target

    @ManyToOne
    @NotNull
    Manager author

    @CreatedDate
    Date dateCreated

    @NotEmpty
    String text



/*    static mapping = {
        sort 'dateCreated': 'asc'
    }*/

    //static transients = ['userService']

   /* def beforeValidate() {
        author = author ?: userService?.currentProfile
    }

    def afterInsert() {
        userService.chat(this)
    }*/
}
