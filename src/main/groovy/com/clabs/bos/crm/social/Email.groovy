package com.clabs.bos.crm.social

import org.hibernate.validator.constraints.NotEmpty

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

/**
 * Created by dmytro on 14.08.14.
 */
@Entity
class Email {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;
    //String subject
    @NotEmpty
    String content

    static hasMany = [bcc: String]

}
