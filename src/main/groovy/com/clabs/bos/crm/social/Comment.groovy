package com.clabs.bos.crm.social

import com.clabs.bos.crm.actors.manager.Manager
import com.clabs.bos.crm.tasks.task.Task
import org.hibernate.validator.constraints.NotEmpty
import org.springframework.data.annotation.CreatedDate

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.validation.constraints.NotNull
@Entity
class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;
    //def userService
    @ManyToOne
    @NotNull
    Manager author
    @CreatedDate
    Date dateCreated

    @NotEmpty
    String text

    /*Relations*/
    @ManyToOne
    Task task

//    public static enum Type {
//        SOCIAL, RESULT
//    }

/*    static mapping = {
        sort 'dateCreated': 'asc'
    }*/

/*    def beforeValidate() {
        author = author ?: userService?.currentProfile
    }

    def afterInsert() {
        userService.sendToAll(this)
    }*/
}
