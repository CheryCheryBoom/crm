package com.clabs.bos.crm.util.event

import com.clabs.bos.crm.actors.company.Company
import com.clabs.bos.crm.actors.contact.Contact
import com.clabs.bos.crm.actors.lead.Lead
import com.clabs.bos.crm.actors.manager.Manager
import com.clabs.bos.crm.sales.deal.Deal
import com.clabs.bos.crm.social.Comment
import com.clabs.bos.crm.tasks.task.Task
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.data.rest.core.config.Projection

/**
 * Created by Den4ik on 25.03.2015.
 */
@RepositoryRestResource(collectionResourceRel = "events", path = "events")
public interface EventRepository extends PagingAndSortingRepository<Event, Long> {
}
@Projection(name = "detail", types = [Event.class])
public interface Detail {
    long getId()

    Manager getAuthor()

    String getClassName()

    Event.EventType getType()

    String getFieldName()

    String getOldValue()

    String getNewValue()

    Lead getLead()

    Manager getManager()

    Contact getContact()

    Company getCompany()

    Task getTask()

    Deal getDeal()

    Comment getComment()

    Date getDateCreated()

}