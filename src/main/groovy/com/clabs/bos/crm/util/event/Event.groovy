package com.clabs.bos.crm.util.event

import com.clabs.bos.crm.actors.company.Company
import com.clabs.bos.crm.actors.lead.Lead
import com.clabs.bos.crm.actors.manager.Manager
import com.clabs.bos.crm.actors.contact.Contact
import com.clabs.bos.crm.sales.deal.Deal
import com.clabs.bos.crm.social.Comment
import com.clabs.bos.crm.tasks.task.Task
import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener

import javax.persistence.Entity
import javax.persistence.EntityListeners
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.validation.constraints.NotNull

@Entity
@EntityListeners([AuditingEntityListener.class])
class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;

    @ManyToOne
    @CreatedBy
    Manager author
    @NotNull
    String className
    @NotNull
    EventType type

    @CreatedDate
    Date dateCreated

    String fieldName
    String oldValue
    String newValue

    @ManyToOne
    Lead lead
    @ManyToOne
    Manager manager
    @ManyToOne
    Contact contact
    @ManyToOne
    Company company
    @ManyToOne
    Task task
    @ManyToOne
    Deal deal
    @ManyToOne
    Comment comment


    /*static mapping = {
        sort 'dateCreated': 'desc'
//        table 'bos_event'
//        id column: 'bos_event_id', name: 'bosEventId'
//        columns {
//            actor
//        }
        //updateFields cascade: 'save-update'
    }*/

    static public enum EventType {
        INSERT, UPDATE, DELETE
    }
}
