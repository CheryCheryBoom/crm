package com.clabs.bos.crm.util.auth

import com.clabs.bos.crm.actors.manager.Manager
import org.springframework.data.repository.CrudRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

/**
 * Created by Den4ik on 30.03.2015.
 */
@RepositoryRestResource
public interface UserRepository extends CrudRepository<User, Long>{
    User findByName(String name);
}