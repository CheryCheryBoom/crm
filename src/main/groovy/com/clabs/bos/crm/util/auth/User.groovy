package com.clabs.bos.crm.util.auth

import com.clabs.bos.crm.actors.manager.Manager
import org.springframework.security.core.userdetails.UserDetails

import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToOne
import javax.validation.constraints.NotNull

/**
 * Created by Den4ik on 30.03.2015.
 */
@Entity
class User {
    @Id
    @GeneratedValue
    Long id

    String name
    String password

    @OneToOne(cascade = CascadeType.PERSIST)
    Manager manager
}
