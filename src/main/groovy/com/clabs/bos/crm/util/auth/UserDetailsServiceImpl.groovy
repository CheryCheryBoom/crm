package com.clabs.bos.crm.util.auth

import com.clabs.bos.crm.actors.company.Company
import com.clabs.bos.crm.actors.contact.Contact
import com.clabs.bos.crm.actors.manager.Manager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.AuthorityUtils
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

/**
 * Created by Den4ik on 30.03.2015.
 */
@Service
class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository

/*    @Autowired
    UserDetailsServiceImpl(UserRepository repo){
        User user = new User(name: 'admin', password: 'admin')
        repo.save(user)
        userRepository = repo
    }*/

    @Override
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        List<GrantedAuthority> auth = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER")
        User user = userRepository.findByName(username)
        if (!user) return null
        return new org.springframework.security.core.userdetails.User(user.name,user.password,true,true,true,true,auth)
    }
}
