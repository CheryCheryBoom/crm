package com.clabs.bos.crm.sales.deal

import com.clabs.bos.crm.actors.lead.Lead
import com.clabs.bos.crm.actors.manager.Manager
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.data.rest.core.config.Projection

/**
 * Created by Den4ik on 25.03.2015.
 */
@RepositoryRestResource(collectionResourceRel = "deal", path = "deal")
public interface DealRepository extends PagingAndSortingRepository<Deal, Long> {
}
@Projection(name = "detail", types = [Deal.class])
public interface Detail {
    long getId()

    String getTitle()

    BigDecimal getSum()

    int getPossibility()

    Lead getLead()

    String getStatus()

    String getType()

    Date getOrientedDate()

    Date getDateCreated()

    Date getLastUpdated()

    Manager getAuthor()
}
@Projection(name = "compact", types = [Deal.class])
public interface Compact {
    long getId()

    String getTitle()
}