package com.clabs.bos.crm.sales.deal

import com.clabs.bos.crm.actors.lead.Lead
import com.clabs.bos.crm.actors.manager.Manager
import com.clabs.bos.crm.sales.Item
import org.hibernate.validator.constraints.NotEmpty
import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener

import javax.persistence.*
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull

/**
 * Тип даних, що представляє Угоду.
 */
@Entity
@EntityListeners([AuditingEntityListener.class])
class Deal {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;

    @NotEmpty
    String title
    @Min(value = 0l)
    BigDecimal sum = 0.0 //initialize because don't know how to render null and fails when saving
    //Currency currency
    @Min(0l)
    @Max(100l)
    int possibility = 0

    @ManyToOne
    @NotNull
    Lead lead
    @NotEmpty
    String status = Status.PROPOSAL.name()
    @NotEmpty
    String type = Type.SALE_SERVICE.name()
    Date orientedDate

    @CreatedDate
    Date dateCreated
    @LastModifiedDate
    Date lastUpdated

    @CreatedBy
    @ManyToOne
    Manager author

    //Set<Item> items = [] as Set<Item>

    public static enum Status {
        /*INITIATE_INTEREST, ANALYZE_NEEDS, PRESENTATION,*/
        PROPOSAL, /*TALKS, WAITING,*/ CONTRACTING, INVOICE, MONEY_IN_THE_BANK, EXECUTING, SUCCESS, FAILURE, WE_CLOSED
    }

    public static enum Type {
        SALE_GOODS, SALE_SERVICE, SALE_COMPLEX, SERVICING, RETURN
    }
}
