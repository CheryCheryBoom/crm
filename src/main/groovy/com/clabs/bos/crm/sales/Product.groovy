package com.clabs.bos.crm.sales

import org.hibernate.validator.constraints.NotEmpty

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull

/**
 * Created by dmytro on 22.07.14.
 */
@Entity
class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;

    @NotEmpty
    String name

    String description

    @NotNull
    @Min(value = 0l)
    BigDecimal price

}
