package com.clabs.bos.crm.sales

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.OneToOne
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull

/**
 * Created by dmytro on 22.07.14.
 */
@Entity
class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;

    @NotNull
    @OneToOne
    Product product
    @Min(value = 1l)
    int quantity
    @NotNull
    String measure = "шт."


/*    static mapping = {
        product cascade: 'save-update'
    }*/
}
