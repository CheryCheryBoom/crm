package com.clabs.bos.crm.tasks

import com.clabs.bos.crm.actors.manager.Manager
import com.clabs.bos.crm.tasks.task.Task

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.persistence.OneToOne
import javax.validation.constraints.NotNull

/**
 * Created by dmytro on 21.07.14.
 */
@Entity
class Participation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;

    @ManyToOne
    @NotNull
    Manager participant
    @NotNull
    boolean watcher

    //String responsibility
    @NotNull
    String role

    @OneToOne
    Task task
}
