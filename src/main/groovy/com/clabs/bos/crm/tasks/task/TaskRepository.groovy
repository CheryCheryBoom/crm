package com.clabs.bos.crm.tasks.task

import com.clabs.bos.crm.actors.company.Company
import com.clabs.bos.crm.actors.contact.bricks.Address
import com.clabs.bos.crm.actors.lead.Lead
import com.clabs.bos.crm.actors.manager.Manager
import com.clabs.bos.crm.sales.deal.Deal
import com.clabs.bos.crm.social.Email
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.data.rest.core.config.Projection

/**
 * Created by Den4ik on 25.03.2015.
 */
@RepositoryRestResource(collectionResourceRel = "task", path = "task")
public interface TaskRepository extends PagingAndSortingRepository<Task, Long> {
}
@Projection(name = "detail", types = [Task.class])
public interface Detail {
    long getId()

    String getTitle()

    String getDescription()

    String getResult()

    Manager getResponsible()

    Manager getAuthor()

    Date getStartDate()

    Date getDueDate()

    Date getDateCompleted()

    String getType()

    String getStatus()

    int getPriority()

    Task getSupertask()

    Lead getLead()

    Company getCompany()

    Deal getDeal()

    Email getMail()

    Address getAddress()

    Date getDateCreated()

    Date getLastUpdated()
}
@Projection(name = "compact", types = [Task.class])
public interface Compact {
    long getId()

    String getTitle()
}