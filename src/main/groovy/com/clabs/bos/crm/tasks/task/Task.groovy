package com.clabs.bos.crm.tasks.task

import com.clabs.bos.crm.actors.company.Company
import com.clabs.bos.crm.actors.lead.Lead
import com.clabs.bos.crm.actors.manager.Manager
import com.clabs.bos.crm.actors.contact.bricks.Address
import com.clabs.bos.crm.sales.deal.Deal
import com.clabs.bos.crm.social.Email
import org.hibernate.validator.constraints.NotEmpty
import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener

import javax.persistence.Entity
import javax.persistence.EntityListeners
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.persistence.OneToOne
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

@Entity
@EntityListeners([AuditingEntityListener.class])
class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;

    @NotEmpty
    String title
    String description
    @Size(min = 1)
    String result

    @CreatedDate
    Date dateCreated
    @LastModifiedDate
    Date lastUpdated

    @ManyToOne
    @NotNull
    Manager responsible

    @ManyToOne
    @NotNull
    @CreatedBy
    Manager author

    @NotNull
    Date startDate
    Date dueDate
    Date dateCompleted

    String type = Type.TASK.name()
    String status = Status.OPENED.name()
    int priority = 10

    /* Relations */
    @ManyToOne
    Task supertask
    @ManyToOne
    Lead lead

    @ManyToOne
    Company company

    @ManyToOne
    Deal deal
    @OneToOne
    Email mail

    @OneToOne
    Address address

    //transient def userService

    //static transients = ['userService']


/*    static mapping = {
        mail cascade: 'save-update'
    }*/

    public static enum Type {
        CALL, MAIL, /*SMS,*/ MEETING, TASK, PROJECT, CALENDAR_EVENT
    }

    public static enum Status {
        OPENED, IN_PROGRESS, IN_PAUSE, PENDING, SUCCESS, CLOSED, FAILURE
    }

/*    def beforeValidate() {
        author = author ?: userService?.currentProfile
    }*/
}
