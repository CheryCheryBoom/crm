package com.clabs.bos.crm.tasks

import com.clabs.bos.crm.actors.manager.Manager
import com.clabs.bos.crm.tasks.task.Task

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.validation.constraints.NotNull

/**
 * Created by dmytro on 02.08.14.
 */
@Entity
class Alarm {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;
    //def userService

    @ManyToOne
    @NotNull
    Manager target
    @NotNull
    Date alarmDate
    @ManyToOne
    @NotNull
    Task task

    /* def afterInsert() {
         userService.send(this)
     }*/
}
