package com.clabs.bos.crm.actors.lead

import com.clabs.bos.crm.actors.company.Company
import com.clabs.bos.crm.actors.contact.Contact
import com.clabs.bos.crm.actors.manager.Manager
import org.springframework.data.repository.CrudRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.data.rest.core.config.Projection

/**
 * Created by Den4ik on 25.03.2015.
 */
@RepositoryRestResource(collectionResourceRel = "lead", path = "lead")
public interface LeadRepository extends CrudRepository<Lead, Long> {
}

@Projection(name = "detail", types = [Lead.class])
public interface Detail {
    long getId()

    int getPriority()

    String getSource()

    Contact getContact()

    String getDescription()

    Manager getAuthor()

    Company getCompany()

    String getPosition()

}

@Projection(name = "compact", types = [Lead.class])
public interface Compact {
    long getId()

    int getPriority()

//        @Value("#{contact.fullName}")
//        String getFullName()

    String getDescription()
}