package com.clabs.bos.crm.actors;

import com.clabs.bos.crm.actors.company.Company;
import com.clabs.bos.crm.actors.lead.Lead;
import com.clabs.bos.crm.actors.manager.Manager;
import org.springframework.data.rest.core.config.Projection;

/**
 * Created by dima on 31.03.15.
 */
@Projection(name = "full", types = {Lead.class})
public interface FullData {
	Company getCompany();

	Manager getAuthor();
}
