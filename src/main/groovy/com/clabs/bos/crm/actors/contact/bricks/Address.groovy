package com.clabs.bos.crm.actors.contact.bricks

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.validation.constraints.Digits
import javax.validation.constraints.Size

/**
 * Представляє собою фізичну вдресу
 * @see com.clabs.bos.crm.domain.actors.Company наразі є лише в Компанії
 * Created by dmytro on 18.07.14.
 */
@Entity
class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;

    @Size(max = 20, min = 1)
    String country

    @Size(max = 30, min = 1)
    String region

    @Size(max = 30)
    String area

    @Size(max = 30, min = 1)
    String locality

    @Size(max = 30)
    String sublocality

    @Size(max = 30, min = 1)
    String street

    @Size(max = 5)
    String streetNumber

    String comment

    String textual

    @Digits(integer = 3, fraction = 6)
    BigDecimal latitude

    @Digits(integer = 3, fraction = 6)
    BigDecimal longitude

    @Override
    String toString() {
        return "$country. $locality. $street, $streetNumber"
    }
}
