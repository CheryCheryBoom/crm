package com.clabs.bos.crm.actors.contact.bricks

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.validation.constraints.Size

/**
 * Тип даних, що містить реквізити для оплати.
 * @see com.clabs.bos.crm.domain.actors.Company містить список таких об’єктів
 * @author dmytro on 22.07.14.
 */
@Entity
class Requisites {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;
    /**
     * Назва реквізитів (для оплати, філіал, тощо)
     */
    @Size(max = 20, min = 1)
    String name
    /**
     * Номер рахунку в банку
     */
    @Size(max = 50, min = 1)
    String accountNumber
    /**
     * Деталі по рахунку (назва, ще щось)
     */
    @Size(max = 50, min = 1)
    String accountDetails
    /**
     * МФО - в пост-радянських країнах - номер банку
     */
    @Size(max = 30, min = 1)
    String bankNumber
    /**
     * ЄДРПОУ - єдиний державний реєстр підприємств та організацій - ідентифікаційний код платника
     */
    @Size(max = 30, min = 1)
    String stateId
}