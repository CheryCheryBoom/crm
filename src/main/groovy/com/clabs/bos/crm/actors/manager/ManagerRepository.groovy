package com.clabs.bos.crm.actors.manager

import com.clabs.bos.crm.actors.company.Company
import com.clabs.bos.crm.actors.contact.Contact
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.data.rest.core.config.Projection

/**
 * Created by Den4ik on 25.03.2015.
 */
@RepositoryRestResource(collectionResourceRel = "manager", path = "manager")
public interface ManagerRepository extends PagingAndSortingRepository<Manager,Long> {
}
@Projection(name = "detail", types = [Manager.class])
public interface Detail {
    long getId()

    Contact getContact()

    Manager getAuthor()

    Company getCompany()

    String getPosition()

    Date getDateCreated()

    Date getLastUpdated()
}
@Projection(name = "compact", types = [Manager.class])
public interface Compact {
    long getId()

    Contact getContact()
}