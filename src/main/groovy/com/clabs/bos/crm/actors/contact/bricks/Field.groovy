package com.clabs.bos.crm.actors.contact.bricks

import org.hibernate.validator.constraints.NotEmpty

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

/**
 * Тип даних,що відображає контактне поле: телефон, емайл, сторінка в соц. мережі тощо, Містить також додатковий опис (домашній, факс, основний, тощо)
 * @author Дмитро Зонов
 */
@Entity
class Field {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;

    @NotEmpty
    String type

    String description

    @NotEmpty
    String value

    public static enum Type {
        PHONE, EMAIL, WEBSITE, SOCIAL, OTHER
    }

    public static enum Description {
        WORKING, HOME, FAX, MAIN, WEBSITE, BLOG, FACEBOOK, TWITTER, LINKEDIN, OTHER
    }

    /**
     * @deprecated
     * Фабрика для поля
     * @param fields
     * @return
     */
    public static Set<Field> make(def fields) {
        def descr = 'MAIN'

        fields.collect {
            switch (it[1]) {
                case Type.PHONE: descr = Description.WORKING
                    break
                case Type.EMAIL: descr = Description.MAIN
                    break
                case Type.WEBSITE: descr = Description.WEBSITE
                    break
                case Type.SOCIAL: descr = Description.FACEBOOK
            }
            new Field(value: it[0], type: it[1].name(), description: descr)
        } as Set<Field>
    }
}
