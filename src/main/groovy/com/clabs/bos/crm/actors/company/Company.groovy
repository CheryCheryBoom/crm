package com.clabs.bos.crm.actors.company

import com.clabs.bos.crm.actors.contact.bricks.Address
import com.clabs.bos.crm.actors.contact.bricks.Field
import com.clabs.bos.crm.actors.contact.bricks.Requisites
import com.clabs.bos.crm.actors.lead.Lead
import com.clabs.bos.crm.actors.manager.Manager
import com.clabs.bos.crm.util.auth.User
import org.hibernate.validator.constraints.NotEmpty
import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener

import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.EntityListeners
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.OneToOne
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

/**
 * Тип даних, що представляє компанію.
 * @see Manager може працювати на компанію
 * @see com.clabs.bos.crm.actors.lead.Lead також може працювати на компанію
 */
@Entity
@EntityListeners([AuditingEntityListener.class])
class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    /**
     * Назва компанії
     */
    @NotEmpty
    @Size(max = 100)
    String title
    /**
     * Додаткові дані по компанії
     */
    @Size(min = 1)
    String description
    /**
     * Тип діяльності компанії
     */
    @NotNull
    @Size(min = 1, max = 5)
    String service
    /**
     * Адреса компанії (може бути null)
     */
    @OneToOne(cascade = CascadeType.PERSIST)
    Address address

    @OneToMany
    Set<Lead> workers

    /**
     * infoFields - список контактних полів компанії;
     * requisites - список реквізитів компанії;
     */
    //static hasMany = [fields: Field, requisites: Requisites]

    @CreatedDate
    Date dateCreated

    @LastModifiedDate
    Date lastUpdated

    @CreatedBy
    @ManyToOne
    Manager authorCreated



    /*static mapping = {
        sort 'title': 'asc'
        address cascade: 'save-update'
        requisites cascade: 'save-update'
    }*/
}