package com.clabs.bos.crm.actors.contact

import com.clabs.bos.crm.actors.contact.bricks.Field
import org.hibernate.validator.constraints.NotEmpty
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener

import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.EntityListeners
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.OneToMany


/**
 * Клас, що представляє собою контактні дані фізичної особи.
 * {@link Manager} та {@link Lead} можуть містити об’єкт цього класу
 */
@Entity
@EntityListeners([AuditingEntityListener.class])
class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;

    @NotEmpty
    String fullName
    /**
     * true - чоловік, інакше - жінка
     */
    boolean male
    /**
     * Масив полів контакту
     */
    //todo cascade save-update
    @OneToMany(cascade = CascadeType.PERSIST)
    Set<Field> fields = [] as Set<Field>

    @CreatedDate
    Date dateCreated

    @LastModifiedDate
    Date lastUpdated

    /**
     * Зі всіх полів електронних адрес вибирає головне
     * @return текстове значення поля (власне електронну адресу)
     */
    def getMainEmail() {
        fields?.find {
            it.description == Field.Description.MAIN.name() && it.type == Field.Type.EMAIL.name()
        }?.value
    }
}
