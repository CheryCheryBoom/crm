package com.clabs.bos.crm.actors.lead

import com.clabs.bos.crm.actors.company.Company
import com.clabs.bos.crm.actors.contact.Contact
import com.clabs.bos.crm.actors.manager.Manager
import org.hibernate.validator.constraints.NotEmpty
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.annotation.CreatedBy
import org.springframework.data.jpa.domain.support.AuditingEntityListener

import javax.persistence.*
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

/**
 * Представляє ліда. З ним, власне, і взаємодіє користувач.
 * @author Дмитро Зонов
 */
@Entity
@EntityListeners([AuditingEntityListener.class])
class Lead {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;
    /**
     * Контактна особа
     */
    @OneToOne(cascade = CascadeType.PERSIST)
    @NotNull
    Contact contact
    /**
     * Менеджер, що створив ліда
     */
    @NotNull
    @CreatedBy
    @ManyToOne
    Manager author
    /**
     * Компанія, яку представляє лід, якщо це юр.особа
     */
    @ManyToOne(cascade = CascadeType.PERSIST)
    Company company

    /**
     * Позиція ліда в компанії
     */
    @Size(min = 1, max = 100)
    String position
    /**
     * Джерело ліда (радіо, інтернет, тощо)
     */
    @NotEmpty
    String source = 'MY_OWN'
    /**
     * Пріоритет ліда (пацан нічого не рішає або дуже зацікавлений пацан)
     */
    @Min(-1l)
    @Max(7l)
    int priority

    /**
     * Додаткові дані про ліда
     */
    String description

    //transient def userService

/*    static constraints = {
        contact unique: true
    }*/

    /*static mapping = {
        sort 'priority': 'asc'
        contact cascade: 'save-update'
        company cascade: 'save-update'
    }*/

    //static transients = ['userService']

    static public enum Source {
        MY_OWN, RECOMMENDATION, EXISTS, INCOME_CALL, SITE, EMAIL, OFFLINE_ADS, OTHER
    }

/*    def beforeValidate() {
        author = author ?: userService?.currentProfile
    }*/

//    public static final detail = Detail
//    public static final compact = Compact


}
