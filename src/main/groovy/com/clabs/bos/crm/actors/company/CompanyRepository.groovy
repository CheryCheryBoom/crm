package com.clabs.bos.crm.actors.company

import com.clabs.bos.crm.actors.contact.bricks.Address
import com.clabs.bos.crm.actors.lead.Lead
import com.clabs.bos.crm.actors.manager.Manager
import com.clabs.bos.crm.util.auth.User
import org.springframework.data.repository.CrudRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.data.rest.core.config.Projection

/**
 * Created by Den4ik on 25.03.2015.
 */
@RepositoryRestResource(collectionResourceRel = "company", path = "company")
public interface CompanyRepository extends CrudRepository<Company, Long> {
}

@Projection(name = "detail", types = [Company.class])
public interface Detail {
    Long getId()

    String getTitle()

    String getDescription()

    String getService()

    Address getAddress()

    Date getDateCreated()

    Date getLastUpdated()

    Manager getAuthorCreated()

    Set<Lead> getWorkers()

}

@Projection(name = "compact", types = [Company.class])
public interface Compact {
    Long getId()

    String getTitle()

    String getDescription()
}