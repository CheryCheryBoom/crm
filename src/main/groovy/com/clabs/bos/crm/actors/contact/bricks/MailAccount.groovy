package com.clabs.bos.crm.actors.contact.bricks

import com.clabs.bos.crm.actors.manager.Manager
import org.hibernate.validator.constraints.Email

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.OneToOne
import javax.validation.constraints.Size

/**
 * Представляє собою набір налаштувань поштового акаунта, щоб користувачі могли надсилати пошту прямо з системи
 * @see Manager має список таких об’єктів. Саме Менеджер відсилає листи.
 * @author dmytro on 30.07.14.
 */
@Entity
class MailAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;

    @Size(max = 50, min = 3)
    @Email
    String username

    @Size(min = 1)
    String password

    @Size(max = 20, min = 1)
    String host

    int port = 465

    Protocol protocol

    @OneToOne
    Manager manager

    public static enum Protocol {
        SSL, TLS
    }
}