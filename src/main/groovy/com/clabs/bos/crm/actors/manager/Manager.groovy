package com.clabs.bos.crm.actors.manager

import com.clabs.bos.crm.actors.company.Company
import com.clabs.bos.crm.actors.contact.Contact
import org.springframework.data.annotation.CreatedBy
import org.springframework.data.jpa.domain.support.AuditingEntityListener

import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.EntityListeners
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.persistence.OneToOne
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

/**
 * Клас, що представляє собою менеджера або просто працівника. Це і є користувачі системою.
 */
@Entity
@EntityListeners([AuditingEntityListener.class])
class Manager {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;
    /**
     * Контактна інформація менеджера
     */
    @OneToOne(cascade = CascadeType.PERSIST)
    Contact contact
    /**
     * Хто створив менеджера
     * (першим реєструється керуючий системою, потім він додає користувачів)
     */
    @ManyToOne
    @CreatedBy
    Manager author
    /**
     * Компанія на яку працює менеджер - батьківська компанія системи
     * (Компанія, що купила систему)
     */
    @ManyToOne
    @NotNull
    Company company

    /**
     * Посада працівника (менеджера)
     */
    @Size(min = 1, max = 100)
    String position


    //def userService

/*    static constraints = {
        contact unique: true
    }*/

/*    static mapping = {
        contact cascade: 'save-update'
    }*/

    /**
     * Перед збереженням менеджера автоматично призначає автором поточного користувача системи, а компанією - компанію, якій належить автор.
     */
/*    def beforeValidate() {
        if (!author) {
            if (userService?.isLoggedIn())
                author = userService?.currentProfile
        } else company = author.company

        if (!company) {
            if (author) company = author.company
            if (!author) company = Company.first()
        }
    }*/

}
