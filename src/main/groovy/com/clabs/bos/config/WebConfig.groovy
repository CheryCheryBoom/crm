package com.clabs.bos.config

import org.springframework.context.annotation.Bean
import org.springframework.data.repository.support.DomainClassConverter
import org.springframework.format.support.FormattingConversionService
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport

/**
 * Created by dima on 01.04.15.
 */
class WebConfig extends WebMvcConfigurationSupport {
    @Bean
    public DomainClassConverter<?> domainClassConverter() {
        return new DomainClassConverter<FormattingConversionService>(mvcConversionService());
    }
}
