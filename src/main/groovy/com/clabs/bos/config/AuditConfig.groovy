package com.clabs.bos.config


import com.clabs.bos.crm.actors.contact.Contact
import com.clabs.bos.crm.actors.manager.Manager
import com.clabs.bos.crm.actors.manager.ManagerRepository
import com.clabs.bos.crm.util.auth.User
import com.clabs.bos.crm.util.auth.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.data.domain.AuditorAware
import org.springframework.data.jpa.repository.config.EnableJpaAuditing
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder

/**
 * Created by Den4ik on 15.04.2015.
 */
@Configuration
@EnableJpaAuditing
public class AuditConfig implements AuditorAware<Manager> {
    @Autowired
    ManagerRepository managerRepository

    @Autowired
    UserRepository userRepository

    @Override
    Manager getCurrentAuditor() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication()
        if (authentication == null || !authentication.isAuthenticated()) {
            Manager system = managerRepository.findOne(1l)
            system
        } else {
            User user = userRepository.findByName(authentication.getName())
            user.manager
        }
    }
}
