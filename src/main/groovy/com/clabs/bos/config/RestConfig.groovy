package com.clabs.bos.config

import com.clabs.bos.crm.actors.FullData
import com.clabs.bos.crm.actors.lead.Lead
import org.springframework.beans.factory.ObjectFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.repository.support.Repositories
import org.springframework.data.rest.core.config.RepositoryRestConfiguration
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration
import org.springframework.validation.Validator
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean

/**
 * Created by dmytro on 24.03.15.
 */
@Configuration
public class RestConfig extends RepositoryRestMvcConfiguration {

    @Bean
    public Validator validator() {
        return new LocalValidatorFactoryBean();
    }

    @Bean
    @Override
    ValidatingRepositoryEventListener validatingRepositoryEventListener(ObjectFactory<Repositories> repositories) {
        ValidatingRepositoryEventListener listener = super.validatingRepositoryEventListener(repositories);
        listener.addValidator("beforeCreate", validator());
        listener.addValidator("beforeSave", validator());
        return listener;
    }

    @Override
    protected void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.setBaseUri("/bos/crud")
        config.projectionConfiguration().addProjection(FullData.class)
        config.useHalAsDefaultJsonMediaType(false)
    }
}